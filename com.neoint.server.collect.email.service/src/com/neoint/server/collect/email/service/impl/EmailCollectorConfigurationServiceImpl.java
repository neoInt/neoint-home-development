/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.email.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;

import org.apache.felix.utils.properties.Properties;
import org.osgi.framework.Constants;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.core.service.NeoIntConfigurationService;

/**
 * This class is for setting and getting the configuration of the File Parser
 * Service.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 1, unit = TimeUnit.SECONDS)
public class EmailCollectorConfigurationServiceImpl implements NeoIntConfigurationService
{
    private ConfigurationAdmin configAdmin;
    private String pid;
    private final Logger logger = LoggerFactory.getLogger(EmailCollectorConfigurationServiceImpl.class.getName());
    private static final String FILEINSTALL_FILE_NAME = "felix.fileinstall.filename";

    /**
     * Returns the configuration for the given property.
     * 
     * @param property
     *            - Property which shall be loaded
     * 
     * @return configuration string of the property
     */
    @Override
    public String getConfiguration(String property)
    {
        Dictionary<String, Object> properties = null;
        String result = "";
        try
        {
            Configuration configuration = configAdmin.getConfiguration(pid);
            configuration.setBundleLocation(null);
            properties = configuration.getProperties();
            if (properties != null)
            {
                result = (String) properties.get(property);
            }
        }
        catch (IOException e)
        {
            logger.error("Get configuration error {}.", e.getMessage());
        }
        return result;
    }

    /**
     * Sets the value for the given property.
     * 
     * @param property
     *            - Property
     * 
     * @param value
     *            - Value
     */
    @Override
    public boolean setConfiguration(String property, String value)
    {
        Dictionary<String, Object> properties = null;
        try
        {
            Configuration configuration = configAdmin.getConfiguration(pid);
            configuration.setBundleLocation(null);
            properties = configuration.getProperties();
            if (properties != null)
            {
                properties.put(property, value);
                configuration.update();
                updateStorage(pid, properties);
            }
            else
            {
                return false;
            }
        }
        catch (IOException e)
        {
            logger.error("Update error {}.", e.getMessage());
        }
        return true;
    }

    /**
     * Sets the value for the properties.
     * 
     * @param properties
     *            - Properties
     */
    @Override
    public boolean setConfigurationProperties(Dictionary<String, Object> properties)
    {
        try
        {
            Configuration configuration = configAdmin.getConfiguration(pid);
            configuration.setBundleLocation(null);
            if (properties != null)
            {
                updateStorage(pid, properties);
                configuration.update();
            }
            else
            {
                return false;
            }
        }
        catch (IOException e)
        {
            logger.error("Update error {}.", e.getMessage());
        }
        return true;
    }

    /**
     * Returns all configuration properties as key values.
     * 
     * @return Configuration properties as key values.
     */
    @Override
    public Dictionary<String, Object> getConfigProperties()
    {
        Dictionary<String, Object> properties = null;
        try
        {
            Configuration configuration = configAdmin.getConfiguration(pid);
            configuration.setBundleLocation(null);
            properties = configuration.getProperties();
        }
        catch (IOException e)
        {
            logger.error("Get configuration properties error {}.", e.getMessage());
        }

        return properties;
    }

    /**
     * Deletes the property from the configuration.
     * 
     * @param property
     *            - Property which shall be deleted
     */
    @Override
    public boolean deleteConfiguration(String property)
    {
        Dictionary<String, Object> properties = null;
        try
        {
            Configuration configuration = configAdmin.getConfiguration(pid);
            configuration.setBundleLocation(null);
            properties = configuration.getProperties();
            if (properties != null)
            {
                properties.remove(property);
                configuration.update();
                updateStorage(pid, properties);
            }
            else
            {
                return false;
            }
        }
        catch (IOException e)
        {
            logger.error("Delete error {}.", e.getMessage());
        }
        return true;
    }

    @SuppressWarnings("unused")
    private boolean updateStorage(String pid, Dictionary<String, Object> props) throws IOException
    {
        File storage = new File(System.getProperty("karaf.etc"));
        if (storage == null)
        {
            logger.error("Config file for PID {} not found", pid);
            return false;
        }
        else
        {
            // get the cfg file
            File cfgFile = new File(storage, pid + ".cfg");
            logger.info("Updating config file {}", cfgFile.getAbsolutePath());
            Configuration cfg = configAdmin.getConfiguration(pid, null);
            // update the cfg file depending of the configuration
            if (cfg != null && cfg.getProperties() != null)
            {
                Object val = cfg.getProperties().get(FILEINSTALL_FILE_NAME);
                try
                {
                    if (val instanceof URL)
                    {
                        cfgFile = new File(((URL) val).toURI());
                    }
                    if (val instanceof URI)
                    {
                        cfgFile = new File((URI) val);
                    }
                    if (val instanceof String)
                    {
                        cfgFile = new File(new URL((String) val).toURI());
                    }
                }
                catch (Exception e)
                {
                    logger.error("Update Storage exception {}", e.getMessage());
                    throw (IOException) new IOException(e.getMessage()).initCause(e);
                }
            }

            // update the cfg file
            Properties properties = new Properties(cfgFile);
            for (Enumeration<String> keys = props.keys(); keys.hasMoreElements();)
            {
                String key = keys.nextElement();
                if (!Constants.SERVICE_PID.equals(key) && !ConfigurationAdmin.SERVICE_FACTORYPID.equals(key)
                        && !FILEINSTALL_FILE_NAME.equals(key))
                {
                    if (props.get(key) != null)
                    {
                        properties.put(key, props.get(key).toString());
                    }
                }
            }
            // remove "removed" properties from the cfg file
            ArrayList<String> propertiesToRemove = new ArrayList<>();
            for (String key : properties.keySet())
            {
                if (props.get(key) == null && !Constants.SERVICE_PID.equals(key)
                        && !ConfigurationAdmin.SERVICE_FACTORYPID.equals(key) && !FILEINSTALL_FILE_NAME.equals(key))
                {
                    propertiesToRemove.add(key);
                }
            }
            for (String key : propertiesToRemove)
            {
                properties.remove(key);
            }
            // save the cfg file
            storage.mkdirs();
            properties.save();
            return true;
        }
    }

    /**
     * The OSGi Configuration Admin.
     * 
     * @return the Configuration Admin
     */
    public ConfigurationAdmin getConfigAdmin()
    {
        return configAdmin;
    }

    /**
     * The OSGi Configuration Admin.
     * 
     * @param configAdmin
     *            - the Configuation Admin
     */
    public void setConfigAdmin(ConfigurationAdmin configAdmin)
    {
        this.configAdmin = configAdmin;
    }

    /**
     * The PID for the configuration.
     * 
     * @return - the persistence ID
     */
    public String getPid()
    {
        return pid;
    }

    /**
     * The PID for the configuration.
     * 
     * @param pid
     *            - the persistence ID
     */
    public void setPid(String pid)
    {
        this.pid = pid;
    }

}
