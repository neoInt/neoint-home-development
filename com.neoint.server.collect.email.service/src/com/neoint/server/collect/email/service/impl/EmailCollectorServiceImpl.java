/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.email.service.impl;

import java.util.concurrent.TimeUnit;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.collect.parser.service.FileParserService;

/**
 * This class receives E-mails with and w/o attachments. The E-mails and
 * attachments will be stored, parsed and the content will be sent to the index
 * service.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class EmailCollectorServiceImpl
{

    private String popHost = "";
    private String imapHost = "";
    private String username = "";
    private String password = "";
    private String storagePath = "";
    private String inboxFolder = "INBOX";
    private int checkInterval = 5;
    private int onlyUnread = 1;
    private FileParserService fileParserService;

    private Scheduler scheduler;

    private final Logger logger = LoggerFactory.getLogger(EmailCollectorServiceImpl.class.getName());

    /**
     * This method is called during startup of the bundle. It reads the
     * configuration and starts the E-mail client for receiving the E-mails.
     * 
     */
    public void startup()
    {
        // Grab the Scheduler instance from the Factory
        try
        {
            if (imapHost.length() > 3 && username.length() > 2 && password.length() > 2 && storagePath.length() > 1)
            {
                scheduler = StdSchedulerFactory.getDefaultScheduler();

                // define the job and tie it to our EmailReceivingHandler class
                JobDetail job = JobBuilder.newJob(EmailCollectorHandler.class).withIdentity("emailJob").build();
                scheduler.getContext().put("popHost", popHost);
                scheduler.getContext().put("imapHost", imapHost);
                scheduler.getContext().put("username", username);
                scheduler.getContext().put("password", password);
                scheduler.getContext().put("storagePath", storagePath);
                scheduler.getContext().put("inboxFolder", inboxFolder);
                scheduler.getContext().put("onlyUnread", onlyUnread);
                scheduler.getContext().put("fileParserService", fileParserService);

                // Trigger the job to run now, and then repeat every 40 seconds
                Trigger trigger = TriggerBuilder.newTrigger().withIdentity("emailTrigger").startNow()
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(checkInterval * 60)
                                .repeatForever())
                        .build();

                scheduler.start();
                // Tell quartz to schedule the job using our trigger
                scheduler.scheduleJob(job, trigger);
            }
            else
            {
                logger.warn("Email Collector Service not configured");
            }
        }
        catch (SchedulerException e)
        {
            logger.error("Error starting sheduler {}", e.getMessage());
        }
    }

    /**
     * This method is called during shutdown of the bundle. It stops the E-mail
     * client.
     * 
     */
    public void shutdown()
    {
        try
        {
            scheduler.shutdown();
        }
        catch (SchedulerException e)
        {
            logger.error("Error shutting down sheduler {}", e.getMessage());
        }
    }

    /**
     * @return the popHost
     */
    public String getPopHost()
    {
        return popHost;
    }

    /**
     * @param popHost
     *            the popHost to set
     */
    public void setPopHost(String popHost)
    {
        this.popHost = popHost;
    }

    /**
     * @return the imapHost
     */
    public String getImapHost()
    {
        return imapHost;
    }

    /**
     * @param imapHost
     *            the imapHost to set
     */
    public void setImapHost(String imapHost)
    {
        this.imapHost = imapHost;
    }

    /**
     * @return the popUser
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * @param popUser
     *            the popUser to set
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * @return the popPassword
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param popPassword
     *            the popPassword to set
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * @return the storagePath
     */
    public String getStoragePath()
    {
        return storagePath;
    }

    /**
     * @param storagePath
     *            the storagePath to set
     */
    public void setStoragePath(String storagePath)
    {
        this.storagePath = storagePath;
    }

    /**
     * @return the inboxFolder
     */
    public String getInboxFolder()
    {
        return inboxFolder;
    }

    /**
     * @param inboxFolder
     *            the inboxFolder to set
     */
    public void setInboxFolder(String inboxFolder)
    {
        this.inboxFolder = inboxFolder;
    }

    /**
     * @return the checkInterval
     */
    public int getCheckInterval()
    {
        return checkInterval;
    }

    /**
     * @param checkInterval
     *            the checkInterval to set
     */
    public void setCheckInterval(int checkInterval)
    {
        this.checkInterval = checkInterval;
    }

    /**
     * @return the onlyUnread
     */
    public int getOnlyUnread()
    {
        return onlyUnread;
    }

    /**
     * @param onlyUnread
     *            the onlyUnread to set
     */
    public void setOnlyUnread(int onlyUnread)
    {
        this.onlyUnread = onlyUnread;
    }

    /**
     * @return the fileParserService
     */
    public FileParserService getFileParserService()
    {
        return fileParserService;
    }

    /**
     * @param fileParserService
     *            the fileParserService to set
     */
    public void setFileParserService(FileParserService fileParserService)
    {
        this.fileParserService = fileParserService;
    }
}
