/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.email.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.Normalizer;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.collect.parser.service.FileParserService;
import com.neoint.server.core.exception.ParserException;
import com.sun.mail.imap.IMAPFolder;

/**
 * This class is the receiving service thread. It checks every x minute about
 * new E-Mails, downloads them including attachments and sends the data to the
 * index service.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class EmailCollectorHandler implements org.quartz.Job
{
    private FileParserService fileParserService;
    private String popHost = "";
    private String imapHost = "";
    private String username = "";
    private String password = "";
    private String storagePath = "";
    private int onlyUnread = 1;
    private String inboxFolder = "INBOX";

    private final Logger logger = LoggerFactory.getLogger(EmailCollectorHandler.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        SchedulerContext schedulerContext = null;
        try
        {
            schedulerContext = context.getScheduler().getContext();
        }
        catch (SchedulerException e1)
        {
            logger.error("Error execute E-Mail job {}", e1.getMessage());
        }
        fileParserService = (FileParserService) schedulerContext.get("fileParserService");
        popHost = (String) schedulerContext.get("popHost");
        imapHost = (String) schedulerContext.get("imapHost");
        username = (String) schedulerContext.get("username");
        password = (String) schedulerContext.get("password");
        storagePath = (String) schedulerContext.get("storagePath");
        inboxFolder = (String) schedulerContext.get("inboxFolder");
        onlyUnread = (int) schedulerContext.get("onlyUnread");

        try
        {
            getMessages();
        }
        catch (Exception e)
        {
            logger.error("Error reading E-Mail messages {}", e.getMessage());
        }
    }

    private void getMessages() throws Exception
    {
        Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");
        Session session = Session.getDefaultInstance(props, null);
        Store store = session.getStore("imaps");
        try
        {
            logger.debug("Connecting to IMAP server {}", imapHost);
            store.connect(imapHost, username, password);

            Folder root = store.getDefaultFolder();
            Folder[] folders = root.list();
            logger.debug("Folders");
            for (int i = 0; i < folders.length; i++)
            {
                logger.debug("\t" + folders[i].getName());
            }

            IMAPFolder folder = (IMAPFolder) store.getFolder(inboxFolder);

            long afterFolderSelectionTime = System.nanoTime();
            int totalNumberOfMessages = 0;
            try
            {
                if (!folder.isOpen())
                {
                    folder.open(Folder.READ_WRITE);
                }

                /*
                 * Now we fetch the message from the IMAP folder in descending
                 * order.
                 *
                 * This way the new mails arrive with the first chunks and older
                 * mails afterwards.
                 */
                long largestUid = folder.getUIDNext() - 1;
                int chunkSize = 500;
                for (long offset = 0; offset < largestUid; offset += chunkSize)
                {
                    long start = Math.max(1, largestUid - offset - chunkSize + 1);
                    long end = Math.max(1, largestUid - offset);

                    /*
                     * The next line fetches the existing messages within the
                     * given range from the server.
                     *
                     * The messages are not loaded entirely and contain hardly
                     * any information. The Message-instances are mostly empty.
                     */
                    long beforeTime = System.nanoTime();
                    Message[] messages = folder.getMessagesByUID(start, end);
                    totalNumberOfMessages += messages.length;
                    logger.debug("Found " + messages.length + " messages (took "
                            + (System.nanoTime() - beforeTime) / 1000 / 1000 + " ms)");

                    /*
                     * If we would access e.g. the subject of a message right
                     * away it would be fetched from the IMAP server lazily.
                     *
                     * Fetching the subjects of all messages one by one would
                     * produce many requests to the IMAP server and take too
                     * much time.
                     *
                     * Instead with the following lines we load some information
                     * for all messages with one single request to save some
                     * time here.
                     */
                    beforeTime = System.nanoTime();
                    // this instance could be created outside the loop as well
                    FetchProfile metadataProfile = new FetchProfile();
                    // load flags, such as SEEN (read), ANSWERED, DELETED, ...
                    metadataProfile.add(FetchProfile.Item.FLAGS);
                    // also load From, To, Cc, Bcc, ReplyTo, Subject and Date
                    metadataProfile.add(FetchProfile.Item.ENVELOPE);
                    // we could as well load the entire messages (headers and
                    // body, including all "attachments")
                    // metadataProfile.add(IMAPFolder.FetchProfileItem.MESSAGE);
                    folder.fetch(messages, metadataProfile);
                    logger.debug("Loaded messages (took " + (System.nanoTime() - beforeTime) / 1000 / 1000 + " ms)");

                    /*
                     * Now that we have all the information we need, let's print
                     * some mails. This should be wicked fast.
                     */
                    beforeTime = System.nanoTime();
                    for (int i = messages.length - 1; i >= 0; i--)
                    {
                        Message message = messages[i];
                        long uid = folder.getUID(message);
                        boolean isRead = message.isSet(Flags.Flag.SEEN);
                        if ((!isRead && onlyUnread == 1) || (isRead && onlyUnread == 0))
                        {
                            String subdirectory = uid + "";
                            File directory = new File(storagePath + "/" + subdirectory);

                            if (!directory.exists())
                            {
                                boolean success = directory.mkdir();
                                if (!success)
                                {
                                    // Directory creation failed
                                    subdirectory = null;
                                }
                            }

                            createMessage(message.getAllRecipients(), message.getFrom(), message
                                    .getSubject(), getTextFromMessage(message), uid + "", subdirectory);

                            Multipart multipart = (Multipart) message.getContent();
                            logger.debug("Multipart count: " + multipart.getCount());

                            handleMultipart(multipart, uid + "", subdirectory);
                        }
                    }
                    logger.debug("Listed message (took " + (System.nanoTime() - beforeTime) / 1000 / 1000 + " ms)");
                }
            }
            finally
            {
                if (folder.isOpen())
                {
                    folder.close(true);
                }
            }

            logger.debug("Listed all " + totalNumberOfMessages + " messages (took "
                    + (System.nanoTime() - afterFolderSelectionTime) / 1000 / 1000 + " ms)");
        }
        finally
        {
            store.close();
        }
    }

    public void handleMultipart(Multipart mp, String fileName, String subdirectory)
            throws MessagingException, IOException, ParserException
    {
        int count = mp.getCount();
        for (int i = 0; i < count; i++)
        {
            BodyPart bp = mp.getBodyPart(i);
            Object content = bp.getContent();
            if (content instanceof InputStream)
            {
                // handle input stream
                InputStream is = (InputStream) content;
                // String attachment =
                // bp.getFileName().replaceAll("[^a-zA-Z0-9.-]", "_");
                String decoded = MimeUtility.decodeText(bp.getFileName());
                String attachment = Normalizer.normalize(decoded, Normalizer.Form.NFC);
                File f;

                String path = storagePath + "/" + fileName + "-" + attachment;
                if (subdirectory != null)
                {
                    path = storagePath + "/" + subdirectory + "/" + fileName + "-" + attachment;
                }
                f = new File(path);
                FileOutputStream fos = new FileOutputStream(f);
                byte[] buf = new byte[4096];
                int bytesRead;
                while ((bytesRead = is.read(buf)) != -1)
                {
                    fos.write(buf, 0, bytesRead);
                }
                fos.close();
                fileParserService.parseFileAndIndex(new URL("file://" + path));
            }
            else if (content instanceof Multipart)
            {
                Multipart mp2 = (Multipart) content;
                handleMultipart(mp2, fileName, subdirectory);
            }
        }
    }

    private void createMessage(Address[] to, Address[] from, String subject, String body, String fileName,
            String subdirectory)
            throws AddressException, MessagingException, FileNotFoundException, IOException, ParserException
    {
        Message message = new MimeMessage(Session.getInstance(System.getProperties()));
        for (Address address : from)
        {
            message.setFrom(new InternetAddress(address.toString()));
        }
        message.setRecipients(Message.RecipientType.TO, to);
        message.setSubject(subject);
        // create the message part
        MimeBodyPart content = new MimeBodyPart();
        // fill message
        content.setText(body);
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(content);
        // integration
        message.setContent(multipart);
        // store file

        String path = "";
        if (subdirectory != null)
        {
            path = storagePath + "/" + subdirectory + "/" + fileName + ".emlx";
        }
        else
        {
            path = storagePath + "/" + fileName + ".emlx";
        }

        message.writeTo(new FileOutputStream(new File(path)));
        fileParserService.parseFileAndIndex(new URL("file://" + path));

    }

    private String getTextFromMessage(Message message) throws Exception
    {
        String result = "";
        if (message.isMimeType("text/plain"))
        {
            result = message.getContent().toString();
        }
        else if (message.isMimeType("multipart/*"))
        {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            result = getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }

    private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws Exception
    {
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++)
        {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain"))
            {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            }
            else if (bodyPart.isMimeType("text/html"))
            {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + html;
            }
            else if (bodyPart.getContent() instanceof MimeMultipart)
            {
                result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
            }
        }
        return result;
    }
}
