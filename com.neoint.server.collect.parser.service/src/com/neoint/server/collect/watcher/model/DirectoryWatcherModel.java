/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.watcher.model;

/**
 * The model class for the directory watcher configuration.
 * 
 * @author uwe
 *
 */
public class DirectoryWatcherModel
{
    private String path = "";
    private boolean recursive = false;

    /**
     * The path which shall be watched.
     * 
     * @return path
     */
    public String getPath()
    {
        return path;
    }

    /**
     * The path which shall be watched.
     * 
     * @param path
     *            - Path
     */
    public void setPath(String path)
    {
        this.path = path;
    }

    /**
     * If the path shall be recursive watched.
     * 
     * @return true or false
     */
    public boolean isRecursive()
    {
        return recursive;
    }

    /**
     * If the path shall be recursive watched.
     * 
     * @param recursive
     *            - true or false
     */
    public void setRecursive(boolean recursive)
    {
        this.recursive = recursive;
    }
}
