/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.watcher.service.impl;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jcabi.aspects.Loggable;
import com.neoint.server.collect.index.service.IndexService;
import com.neoint.server.collect.parser.service.FileParserService;
import com.neoint.server.collect.parser.service.FileService;
import com.neoint.server.collect.watcher.model.DirectoryWatcherModel;

/**
 * This service watches directories. If any changes on the directory and
 * subdirectories appears (new, modified, deleted) the service calls the file
 * parser service to parse / reparse the content. On deletion the service calls
 * the indexer service to remove the index for the deleted file or directory.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class DirectoryWatcherServiceImpl
{
    private String directories = "";
    private List<DirectoryWatcherModel> directoryList = null;
    private FileService fileService;
    private FileParserService fileParserService;
    private IndexService indexService;

    private List<WatchHandler> threadList;
    private final Logger logger = LoggerFactory.getLogger(DirectoryWatcherServiceImpl.class.getName());

    /**
     * This method is called during startup of the bundle. It reads the
     * configuration which directories shall be watched and if recursive. The it
     * creates for each a directory watcher. If something changes the file
     * parser service is being called.
     * 
     */
    public void startup()
    {
        threadList = new ArrayList<WatchHandler>();
        if (directoryList != null)
        {
            for (DirectoryWatcherModel watcher : directoryList)
            {
                try
                {
                    Path dir = Paths.get(watcher.getPath());
                    WatchHandler wh = new WatchHandler(dir, watcher.isRecursive(), fileParserService, indexService);
                    wh.start();
                    threadList.add(wh);
                }
                catch (IOException e)
                {
                    logger.error("Could not start directory watcher for directory {} with exception {}", watcher
                            .getPath(), e.getMessage());
                }
            }
        }
        else
        {
            logger.info("Directory List not configured.");
        }
    }

    /**
     * This method is called during shutdown of the bundle. It stops all
     * directory watchers.
     * 
     */
    public void shutdown()
    {
        for (WatchHandler thread : threadList)
        {
            thread.stopThread();
            thread.interrupt();
        }
    }

    /**
     * The directories which shall be watched. As JSON format
     * {"directories":[{"path":"/path1","recursive":true},{"path":"/some/path2",
     * "recursive":false}]}
     * 
     * @return directories
     */
    public String getDirectories()
    {
        return directories;
    }

    /**
     * The directories which shall be watched.
     * 
     * @return directories
     */
    public List<DirectoryWatcherModel> getDirectoriesAsList()
    {
        return directoryList;
    }

    /**
     * The directories which shall be watched. As JSON format
     * {"directories":[{"path":"/path1","recursive":true},{"path":"/some/path2",
     * "recursive":false}]}
     * 
     * @param directories
     *            as JSON
     */
    public void setDirectories(String directories)
    {
        this.directories = directories;
        Gson gson = new Gson();
        if (directories.length() > 0)
        {
            Type collectionType = new TypeToken<Collection<DirectoryWatcherModel>>()
            {
            }.getType();
            directoryList = gson.fromJson(directories, collectionType);
        }
        else
        {
            logger.info("Directory List not configured.");
        }
    }

    /**
     * The File Service
     * 
     * @return file service
     */
    public FileService getFileService()
    {
        return fileService;
    }

    /**
     * The File Service
     * 
     * @param fileService
     *            - file service
     */
    public void setFileService(FileService fileService)
    {
        this.fileService = fileService;
    }

    /**
     * The File Parser Service
     * 
     * @return file parser service
     */
    public FileParserService getFileParserService()
    {
        return fileParserService;
    }

    /**
     * The File Parser Service
     * 
     * @param fileParserService
     *            - file parser service
     */
    public void setFileParserService(FileParserService fileParserService)
    {
        this.fileParserService = fileParserService;
    }

    /**
     * The Index Service
     * 
     * @return index service
     */
    public IndexService getIndexService()
    {
        return indexService;
    }

    /**
     * The Index Service
     * 
     * @param indexService
     *            - index service
     */
    public void setIndexService(IndexService indexService)
    {
        this.indexService = indexService;
    }
}
