/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.watcher.service.impl;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neoint.server.collect.index.service.IndexService;
import com.neoint.server.collect.parser.service.FileParserService;

/**
 * This class is the watcher thread.
 * 
 * @author uwe
 *
 */
public class WatchHandler extends Thread
{
    private WatchService watchService;
    private final Map<WatchKey, Path> keys;
    private final boolean recursive;
    private boolean trace = false;
    private final Logger logger = LoggerFactory.getLogger(WatchHandler.class.getName());
    private boolean running;
    private FileParserService fileParserService;
    private IndexService indexService;
    private Path dir;

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event)
    {
        return (WatchEvent<T>) event;
    }

    public WatchHandler(Path dir, boolean recursive, FileParserService fileParserService, IndexService indexService)
            throws IOException
    {
        this.watchService = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<WatchKey, Path>();
        this.recursive = recursive;
        this.dir = dir;

        if (recursive)
        {
            logger.info("Scanning {}.", dir);
            registerAll(dir);
            logger.info("Scanning Done.");
        }
        else
        {
            register(dir);
        }

        // enable trace after initial registration
        this.trace = true;
        this.fileParserService = fileParserService;
        this.indexService = indexService;
        running = true;
    }

    /**
     * Register the given directory with the WatchService
     */
    private void register(Path dir) throws IOException
    {
        WatchKey key = dir.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        if (trace)
        {
            Path prev = keys.get(key);
            if (prev == null)
            {
                logger.info("Register directory: {}", dir);
            }
            else
            {
                if (!dir.equals(prev))
                {
                    logger.info("Update Directory: {} -> {}", prev, dir);
                }
            }
        }
        keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException
    {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>()
        {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
            {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @Override
    public void run()
    {
        while (running)
        {
            // wait for key to be signalled
            WatchKey key;
            try
            {
                key = watchService.take();
            }
            catch (InterruptedException e)
            {
                logger.error("Watcher Interrupt Exception {}", e.getMessage());
                return;
            }

            Path dir = keys.get(key);
            if (dir == null)
            {
                logger.warn("WatchKey not recognized!!");
                continue;
            }

            for (WatchEvent<?> event : key.pollEvents())
            {
                WatchEvent.Kind kind = event.kind();

                // TBD - provide example of how OVERFLOW event is handled
                if (kind == OVERFLOW)
                {
                    continue;
                }

                // Context for directory entry event is the file name of entry
                WatchEvent<Path> ev = cast(event);
                Path name = ev.context();
                Path child = dir.resolve(name);

                try
                {
                    if (!Files.isDirectory(child) && !Files.isHidden(child) && !child.toString().startsWith(".")
                            && !FilenameUtils.getExtension(child.toString()).equalsIgnoreCase("app"))
                    {
                        // Some changes has been found -> Handle that
                        if (event.kind() == ENTRY_CREATE)
                        {
                            // New file has been added -> index the file
                            fileParserService.parseFileAndIndex(new URL("file://" + child.toString()));
                        }
                        else if (event.kind() == ENTRY_MODIFY)
                        {
                            // File has been modified -> index the file and
                            // update
                            // the index
                            fileParserService.parseFileAndIndex(new URL("file://" + child.toString()));
                        }
                        else if (event.kind() == ENTRY_DELETE)
                        {
                            // File has been deleted -> delete the index entry
                            indexService.deleteIndexContent(child.toString());
                        }
                    }
                    else
                    {
                        logger.info("Directory or hidden file {} changed", child.toString());
                    }
                }
                catch (Exception ex)
                {
                    logger.error("Error calling File Parser Service {}", ex.getMessage());
                }
                // if directory is created, and watching recursively, then
                // register it and its sub-directories
                if (recursive && (kind == ENTRY_CREATE))
                {
                    try
                    {
                        if (Files.isDirectory(child, java.nio.file.LinkOption.NOFOLLOW_LINKS))
                        {
                            registerAll(child);
                        }
                    }
                    catch (IOException e)
                    {
                        // ignore to keep sample readbale
                        logger.warn("WatchHandler recusive exception {}", e.getMessage());
                    }
                }
            }

            // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid)
            {
                keys.remove(key);

                // all directories are inaccessible
                if (keys.isEmpty())
                {
                    break;
                }
            }
        }
        logger.info("Exit Watcher for directory {}", dir.toString());
    }

    public void stopThread()
    {
        running = false;
    }
}
