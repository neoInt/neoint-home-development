/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.parser.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FilenameUtils;

import com.jcabi.aspects.Loggable;
import com.neoint.server.collect.parser.service.FileService;

/**
 * This service is for directory listings and finding files.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class FileServiceImpl implements FileService
{
    private List<File> files = new ArrayList<File>();
    private List<String> directories = new ArrayList<String>();

    /**
     * This method returns a list of files.
     * 
     * @param directoryName
     *            - Directory which shall be searched
     * 
     * @param recursive
     *            - If search is recursive
     * 
     * @param excludedFiles
     *            - Which files shall be excluded. The file suffix e.g. exe,dll
     *            etc has to be given.
     * 
     * @return List of files
     */
    public List<File> getFiles(String directoryName, boolean recursive, List<String> excludedFiles)
    {
        files = new ArrayList<File>();
        getFilesList(directoryName, recursive, excludedFiles);
        return files;
    }

    /**
     * This method returns a list of directories.
     * 
     * @param directoryName
     *            - Directory which shall be searched
     * 
     * @param recursive
     *            - If search is recursive
     * 
     * @return List of directory paths
     */
    public List<String> getDirectories(String directoryName, boolean recursive)
    {
        directories = new ArrayList<String>();
        directories.add(directoryName);
        getDirectoryList(directoryName, recursive);
        return directories;
    }

    private void getDirectoryList(String directoryName, boolean recursive)
    {
        File directory = new File(directoryName);

        // get all the files from a directory
        File[] fList = directory.listFiles();
        if (fList != null)
        {
            for (File file : fList)
            {
                if (file.isDirectory() && !file.getName().startsWith(".") && !file.isHidden()
                        && !FilenameUtils.getExtension(file.getAbsolutePath()).equalsIgnoreCase("app"))
                {
                    directories.add(file.getAbsolutePath());
                    if (recursive)
                    {
                        getDirectoryList(file.getAbsolutePath(), recursive);
                    }
                }
            }
        }
    }

    private void getFilesList(String directoryName, boolean recursive, List<String> excludedFiles)
    {
        File directory = new File(directoryName);

        // get all the files from a directory
        File[] fList = directory.listFiles();
        if (fList != null)
        {
            for (File file : fList)
            {
                if (file.isFile()
                        && !excludedFiles.contains(FilenameUtils.getExtension(file.getAbsolutePath()).toLowerCase())
                        && !file.getName().startsWith(".") && !file.isHidden())
                {
                    files.add(file);
                }
                else if (file.isDirectory() && !file.getName().startsWith(".") && !file.isHidden()
                        && !FilenameUtils.getExtension(file.getAbsolutePath()).equalsIgnoreCase("app"))
                {
                    if (recursive)
                    {
                        getFilesList(file.getAbsolutePath(), recursive, excludedFiles);
                    }
                }
            }
        }
    }
}
