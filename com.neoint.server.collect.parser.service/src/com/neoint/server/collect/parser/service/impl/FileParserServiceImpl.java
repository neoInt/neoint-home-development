/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.parser.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FilenameUtils;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.ContentHandlerDecorator;
import org.osgi.service.event.EventAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.jcabi.aspects.Loggable;
import com.neoint.server.collect.index.model.ContentEvent;
import com.neoint.server.collect.index.model.ContentMetadata;
import com.neoint.server.collect.parser.service.FileParserService;
import com.neoint.server.collect.parser.service.FileService;
import com.neoint.server.core.exception.ParserException;
import com.neoint.server.core.model.ErrorCodes;
import com.neoint.server.core.model.SearchConstants;

/**
 * This class implements the File Parser Interface. It parses the file and
 * returns the content.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class FileParserServiceImpl implements FileParserService
{
    private static final String FILE_PREFIX_PROTOCOL = "file";
    private static final String FILE_PREFIX = "file://";
    private static final String NEOINT_INDEX = "com/neoint/collect/index";
    private EventAdmin eventAdmin;
    private final Logger logger = LoggerFactory.getLogger(FileParserServiceImpl.class.getName());
    private int parserMaxContentLength = 100000;
    private String parserFileExtContentExclude = "";
    private List<String> parserFileExtContentExcludeList = new ArrayList<String>();
    private FileService fileService;
    private AutoDetectParser parser = new AutoDetectParser();
    private int parserFileMaxParallelParsing = 4;

    /**
     * This method parses all files from the given directory. If recursive is
     * true all subdirectories will be scanned to. The parsed content will be
     * sent as index events.
     * 
     * @param path
     *            - path to a directory on which the service has server has
     *            access.
     * 
     * @param recursive
     *            - true if the directory with all subdirectories shall be
     *            scanned.
     *
     */
    @Override
    public void parseFilesFromDirectoryAndIndex(String path, boolean recursive) throws ParserException
    {
        List<File> files = fileService.getFiles(path, recursive, parserFileExtContentExcludeList);
        parseFilesFromDirectoryAndIndex(files, null);
    }

    /**
     * This method parses all files from the given directory. If recursive is
     * true all subdirectories will be scanned to. The parsed content will be
     * sent as index events. It can be used for streaming the progress.
     * 
     * @param path
     *            - path to a dircetory on which the service has server has
     *            access.
     * 
     * @param recursive
     *            - true if the dirctory with all subdirectories shall be
     *            scanned.
     *
     * @param writer
     *            - a writer where the output progress can be written. The
     *            output will be written JSON String {"progress":
     *            "value as percent"
     *            ,"status":"started/running/finished","filesParsed": parsed
     *            number ,"filesTotal":total number}
     */
    @Override
    public void parseFilesFromDirectoryAndIndex(String path, boolean recursive, Writer writer) throws ParserException
    {
        List<File> files = fileService.getFiles(path, recursive, parserFileExtContentExcludeList);
        parseFilesFromDirectoryAndIndex(files, writer);
    }

    /**
     * This method parses a file from the given URL. The supported URL protocols
     * are:
     * <ul>
     * <li>HTTP(s) (e.g. http://username:password@hostname:port/path/to/file)
     * </li>
     * <li>FTP (ftp://username:password@hostname/path/to/file)</li>
     * <li>File (file:///path/to/mounted/file)</li>
     * </ul>
     * 
     * The parsed content will be sent to the index.
     * 
     * @param url
     *            - The URL of the file which shall be indexed.
     * 
     * @return returns true if parsing worked and event could be sent.
     */
    @Override
    public boolean parseFileAndIndex(URL url) throws ParserException
    {
        if (url.toString().startsWith(FILE_PREFIX) || url.getProtocol().equalsIgnoreCase(FILE_PREFIX_PROTOCOL))
        {
            File file = new File(url.getPath());
            Dictionary<String, Object> properties = parseFile(file);
            if (properties != null)
            {
                ContentEvent contentEvent = new ContentEvent(NEOINT_INDEX, properties);
                eventAdmin.sendEvent(contentEvent);
            }
            else
            {
                logger.error("Error sending index for file {}", url);
                return false;
            }
        }
        else
        {
            throw new ParserException("Not implemented yet", ErrorCodes.GENERAL_ERROR);
        }
        return true;
    }

    /**
     * This method parses a file from the given URL. The supported URL protocols
     * are:
     * <ul>
     * <li>HTTP(s) (e.g. http://username:password@hostname:port/path/to/file)
     * </li>
     * <li>FTP (ftp://username:password@hostname/path/to/file)</li>
     * <li>File (file:///path/to/mounted/file)</li>
     * </ul>
     * 
     * The parsed content will be returned.
     * 
     * @param url
     *            - The URL of the file which shall be indexed.
     * 
     * @return returns the content as JSON
     * 
     */
    @Override
    public String parseFile(URL url) throws ParserException
    {
        String result = "{}";
        if (url.toString().startsWith(FILE_PREFIX) || url.getProtocol().equalsIgnoreCase(FILE_PREFIX_PROTOCOL))
        {
            File file = new File(url.getPath());
            Dictionary<String, Object> properties = parseFile(file);
            Gson gson = new Gson();
            result = gson.toJson(properties).toString();
        }
        else
        {
            throw new ParserException("Not implemented yet", ErrorCodes.GENERAL_ERROR);
        }

        return result;
    }

    /**
     * Event Admin
     * 
     * @return event Admin
     */
    public EventAdmin getEventAdmin()
    {
        return eventAdmin;
    }

    /**
     * Event Admin
     * 
     * @param eventAdmin
     *            - The OSGi Event Admin
     */
    public void setEventAdmin(EventAdmin eventAdmin)
    {
        this.eventAdmin = eventAdmin;
    }

    /**
     * Maximum content length limitation for parsing.
     * 
     * @return Max content length
     */
    public int getParserMaxContentLength()
    {
        return parserMaxContentLength;
    }

    /**
     * Maximum content length limitation for parsing.
     * 
     * @param parserMaxContentLength
     *            - Max content length
     */
    public void setParserMaxContentLength(int parserMaxContentLength)
    {
        this.parserMaxContentLength = parserMaxContentLength;
    }

    /**
     * This contains the file extensions which shall be excluded during parsing.
     * 
     * @return comma separated of file extensions
     */
    public String getParserFileExtContentExclude()
    {
        return parserFileExtContentExclude;
    }

    /**
     * This contains the file extensions which shall be excluded during parsing.
     * 
     * @param parserFileExtContentExclude
     *            - comma separated String of file extensions
     */
    public void setParserFileExtContentExclude(String parserFileExtContentExclude)
    {
        this.parserFileExtContentExclude = parserFileExtContentExclude;
        String[] extList = parserFileExtContentExclude.split(",");
        parserFileExtContentExcludeList = Arrays.asList(extList);
    }

    /**
     * This returns the maximum parallel threads for parsing of a file list.
     * 
     * @return the parserFileMaxParallelParsing
     */
    public int getParserFileMaxParallelParsing()
    {
        return parserFileMaxParallelParsing;
    }

    /**
     * This sets the maximum parallel threads for parsing of a file list.
     * 
     * @param parserFileMaxParallelParsing
     *            the parserFileMaxParallelParsing to set
     */
    public void setParserFileMaxParallelParsing(int parserFileMaxParallelParsing)
    {
        this.parserFileMaxParallelParsing = parserFileMaxParallelParsing;
    }

    /**
     * This is the private method which does all the parsing and sending index
     * event
     * 
     * @param files
     *            - List of files
     * 
     * @param writer
     *            - Writer for writing progress
     * 
     */
    private void parseFilesFromDirectoryAndIndex(List<File> files, Writer writer)
    {
        ExecutorService executor = Executors.newFixedThreadPool(parserFileMaxParallelParsing);

        int i = 0;
        if (writer != null)
        {
            try
            {
                writer.write("{\"progress\":0,\"status\":\"started\",\"filesParsed\":0,\"filesTotal\":" + files.size()
                        + "}");
                writer.flush();
            }
            catch (IOException e)
            {
                logger.warn("Error writing progress for status started");
            }
        }

        for (File file : files)
        {
            logger.info("Parsing file {} of files {}", i, files.size());

            Dictionary<String, Object> properties = parseFile(file);
            if (properties != null)
            {
                ContentEvent contentEvent = new ContentEvent(NEOINT_INDEX, properties);
                eventAdmin.sendEvent(contentEvent);
            }
            else
            {
                logger.error("Error sending index for file {}", file.getAbsolutePath());
            }

            if (writer != null)
            {
                try
                {
                    int percentage = (int) ((i * 100.0f) / files.size());
                    writer.write("{\"progress\":" + percentage + ",\"status\":\"running\",\"filesParsed\":" + i
                            + ",\"filesTotal\":" + files.size() + "}");
                    writer.flush();
                }
                catch (IOException e)
                {
                    logger.warn("Error writing progress: Exception {} after parsing file {}", e.getMessage(), file
                            .getAbsolutePath());
                    // set the writer to null so it will skip trying to write
                    // the progress.
                    writer = null;
                }
            }
            i++;
        }
        if (writer != null)
        {
            try
            {
                writer.write("{\"progress\":100,\"status\":\"finished\",\"filesParsed\":" + i + ",\"filesTotal\":"
                        + files.size() + "}");
                writer.flush();
            }
            catch (IOException e)
            {
                logger.warn("Error writing progress for status finished");
            }
        }
    }

    private Dictionary<String, Object> parseFile(File file)
    {
        logger.info("Parsing file: {}", file.getAbsolutePath());
        Metadata metadata = new Metadata();
        // BodyContentHandler handler = new
        // BodyContentHandler(parserMaxContentLength);
        final List<String> chunks = new ArrayList<>();
        chunks.add("");
        ContentHandlerDecorator handler = new ContentHandlerDecorator()
        {
            @Override
            public void characters(char[] ch, int start, int length)
            {
                String lastChunk = chunks.get(chunks.size() - 1);
                String thisStr = new String(ch, start, length);
                if (thisStr.contains(" "))
                {
                    if (lastChunk.length() + length > 10000)
                    {
                        chunks.add(thisStr);
                    }
                    else
                    {
                        chunks.set(chunks.size() - 1, lastChunk + thisStr);
                    }
                }
            }
        };

        try (InputStream stream = new FileInputStream(file))
        {
            parser.parse(stream, handler, metadata);

            Path filePath = Paths.get(file.getAbsolutePath());
            BasicFileAttributes attr = Files.readAttributes(filePath, BasicFileAttributes.class);

            ContentMetadata contentMetadata = new ContentMetadata();
            if (metadata.getDate(TikaCoreProperties.CREATED) != null)
            {
                contentMetadata.setCreateDate(metadata.getDate(TikaCoreProperties.CREATED));
            }
            else
            {
                contentMetadata.setCreateDate(new Date(attr.creationTime().toMillis()));
            }
            if (metadata.getDate(TikaCoreProperties.MODIFIED) != null)
            {
                contentMetadata.setModifiedDate(metadata.getDate(TikaCoreProperties.MODIFIED));
            }
            else
            {
                contentMetadata.setModifiedDate(new Date(file.lastModified()));
            }

            if (metadata.get(TikaCoreProperties.TITLE) != null)
            {
                contentMetadata.setTitle(metadata.get(TikaCoreProperties.TITLE));
            }
            if (metadata.get(TikaCoreProperties.DESCRIPTION) != null)
            {
                contentMetadata.setDescription(metadata.get(TikaCoreProperties.DESCRIPTION));
            }
            if (metadata.get(TikaCoreProperties.TYPE) != null)
            {
                contentMetadata.setType(metadata.get(TikaCoreProperties.TYPE));
            }
            contentMetadata.setReferenceUri(FILE_PREFIX + file.getAbsolutePath());
            contentMetadata.setId(file.getAbsolutePath());

            String[] metadataNames = metadata.names();
            Map<String, Object> metadataProperties = new HashMap<String, Object>();

            for (String name : metadataNames)
            {
                String key = name.replaceAll("[\"\'=.]", "");
                metadataProperties.put(key, metadata.get(name));
            }
            contentMetadata.setProperties(metadataProperties);

            Dictionary<String, Object> properties = new Hashtable<String, Object>();
            properties.put(SearchConstants.METADATA, contentMetadata);
            StringBuilder content = new StringBuilder();
            if (!parserFileExtContentExcludeList
                    .contains(FilenameUtils.getExtension(file.getAbsolutePath()).toLowerCase()))
            {
                for (String chunk : chunks)
                {
                    content.append(chunk);
                }
                logger.info("Content size length: {}", content.length());
                properties.put(SearchConstants.CONTENT, content.toString());
            }
            else
            {
                logger.info("Excluded file {}. Content not parsed.", file.getAbsolutePath());
                properties.put(SearchConstants.CONTENT, "");
            }
            logger.debug("Content {} for file {}", content, file.getAbsolutePath());
            logger.info("Metadata {} for file {}", contentMetadata.toString(), file.getAbsolutePath());
            return properties;
        }
        catch (Exception ex)
        {
            logger.error("Parsing Excpetion {} for file {} ", ex.getMessage(), file.getAbsolutePath());
        }
        catch (Throwable t)
        {
            logger.error("Parsing error {} for file {} ", t.getMessage(), file.getAbsolutePath());
        }
        return null;
    }

    /**
     * The File Service
     * 
     * @return file service
     */
    public FileService getFileService()
    {
        return fileService;
    }

    /**
     * The File Service
     * 
     * @param fileService
     *            - file service
     */
    public void setFileService(FileService fileService)
    {
        this.fileService = fileService;
    }
}
