/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.parser.service.remote.rest;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.collect.parser.service.FileParserService;
import com.neoint.server.core.exception.ParserException;
import com.neoint.server.core.service.NeoIntConfigurationService;

/**
 * This is the RESTful remote interface of the File Parser Service.
 * 
 * @author Uwe Vass
 * 
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class FileParserRemoteService
{
    private static final int HTTP_CODE_200 = 200;
    private static final int HTTP_CODE_500 = 500;
    private static final int HTTP_CODE_400 = 400;
    private final Logger logger = LoggerFactory.getLogger(FileParserRemoteService.class.getName());
    private FileParserService fileParserService = null;
    private NeoIntConfigurationService fileParserConfigService = null;

    /**
     * This operation is for parsing a directory manually.
     * 
     * Input JSON String:
     * 
     * { "path":"/directory/to/parse", "recursive": "true" or false" }
     * 
     * Result JSON String:
     * 
     * {"progress":"value as percentage"
     * ,"status":"started/running/finished","filesParsed": parsed number
     * ,"filesTotal":total number}
     * 
     * @param parseMessage
     *            - parser message as JSON
     *
     * @return Stream of the parsing process
     */
    @POST
    @Produces("application/json; charset=UTF-8")
    public final StreamingOutput parseDirectory(final String parseMessage)
    {
        final JSONObject parseMessageJson;
        if (parseMessage.length() == 0)
        {
            logger.error("parseDirectory: Empty message");
            return null;
        }
        try
        {
            parseMessageJson = new JSONObject(parseMessage);
        }
        catch (JSONException ex)
        {
            logger.error("parseDirectory: JSON Exeption for {} message {}", ex.getMessage(), parseMessage);
            return null;
        }

        StreamingOutput stream = new StreamingOutput()
        {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException
            {
                Writer writer = new BufferedWriter(new OutputStreamWriter(os));
                try
                {
                    fileParserService.parseFilesFromDirectoryAndIndex(parseMessageJson
                            .getString("path"), parseMessageJson.getBoolean("recursive"), writer);
                }
                catch (ParserException | JSONException e)
                {
                    logger.error("parseDirectory: Parsing Excpetion {} for message {} ", e.getMessage(), parseMessage);
                }
            }
        };

        return stream;
    }

    /**
     * This operation returns the configuration of the excluded files.
     * 
     * @return JSON {"excludedFiles":"CSV string"}
     */
    @GET
    @Path("/settings/excludedfiles")
    @Produces("application/json; charset=UTF-8")
    public final Response getExcludedFiles()
    {
        JSONObject result = new JSONObject();
        String excludedFiles = "";
        try
        {
            excludedFiles = fileParserConfigService.getConfiguration("parser.file.ext.content.exclude");
            result.put("excludedFiles", excludedFiles);
        }
        catch (JSONException e)
        {
            logger.error("getExcludedFiles: JSON Excpetion {} for excluded files {} ", e.getMessage(), excludedFiles);
            return Response.status(HTTP_CODE_500).entity(result.toString()).build();
        }

        return Response.status(HTTP_CODE_200).entity(result.toString()).build();
    }

    /**
     * This operation sets the configuration of the excluded files.
     * 
     * @param parseMessage
     *            - JSON {"excludedFiles":"CSV string"}
     * 
     * @return JSON {"excludedFiles":"CSV string"}
     */
    @POST
    @Path("/settings/excludedfiles")
    @Produces("application/json; charset=UTF-8")
    public final Response setExcludedFiles(final String configMessage)
    {
        JSONObject result = new JSONObject();
        final JSONObject configMessageJson;
        String excludedFiles = "";
        if (configMessage.length() == 0)
        {
            logger.error("setExcludedFiles: Empty message");
            return Response.status(HTTP_CODE_400).entity(result.toString()).build();
        }

        try
        {
            configMessageJson = new JSONObject(configMessage);
            fileParserConfigService
                    .setConfiguration("parser.file.ext.content.exclude", configMessageJson.getString("excludedFiles"));
            excludedFiles = fileParserConfigService.getConfiguration("parser.file.ext.content.exclude");
            result.put("excludedFiles", excludedFiles);
        }
        catch (Exception e)
        {
            logger.error("setExcludedFiles: JSON Excpetion {} for message {} and excluded files {} ", e
                    .getMessage(), configMessage, excludedFiles);
            return Response.status(HTTP_CODE_500).entity(result.toString()).build();
        }

        return Response.status(HTTP_CODE_200).entity(result.toString()).build();
    }

    /**
     * The File Parser Service
     * 
     * @return file parser service
     */
    public FileParserService getFileParserService()
    {
        return fileParserService;
    }

    /**
     * The File Parser Service
     * 
     * @param fileParserService
     *            - file parser service
     */
    public void setFileParserService(FileParserService fileParserService)
    {
        this.fileParserService = fileParserService;
    }

    /**
     * The FileParserConfigService
     * 
     * @return FileParserConfigService
     */
    public NeoIntConfigurationService getFileParserConfigService()
    {
        return fileParserConfigService;
    }

    /**
     * The FileParserConfigService
     * 
     * @param fileParserConfigService
     *            - file parser config service
     */
    public void setFileParserConfigService(NeoIntConfigurationService fileParserConfigService)
    {
        this.fileParserConfigService = fileParserConfigService;
    }
}
