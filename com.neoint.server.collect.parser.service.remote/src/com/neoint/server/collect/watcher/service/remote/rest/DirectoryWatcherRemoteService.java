/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.watcher.service.remote.rest;

import java.util.concurrent.TimeUnit;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.core.service.NeoIntConfigurationService;

/**
 * This is the RESTful remote interface of the Directory Watcher Service.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class DirectoryWatcherRemoteService
{
    private static final int HTTP_CODE_200 = 200;
    private static final int HTTP_CODE_400 = 400;
    private final Logger logger = LoggerFactory.getLogger(DirectoryWatcherRemoteService.class.getName());
    private NeoIntConfigurationService directoryWatcherConfigService = null;

    /**
     * This operation returns the configuration of the watched directories.
     * 
     * @return JSON {"directories":[{"path":"/path1","recursive":true},{"path":
     *         "/some/path2", "recursive":false}]}
     */
    @GET
    @Path("/settings/directories")
    @Produces("application/json; charset=UTF-8")
    public final Response getWatchedDirectories()
    {
        String directories = "";
        directories = directoryWatcherConfigService.getConfiguration("directories.watched");

        return Response.status(HTTP_CODE_200).entity(directories).build();
    }

    /**
     * This operation sets the configuration of the watched directories.
     * 
     * @param parseMessage
     *            - JSON
     *            {"directories":[{"path":"/path1","recursive":true},{"path":
     *            "/some/path2", "recursive":false}]}
     * 
     * @return JSON {"directories":[{"path":"/path1","recursive":true},{"path":
     *         "/some/path2", "recursive":false}]}
     */
    @POST
    @Path("/settings/directories")
    @Produces("application/json; charset=UTF-8")
    public final Response setWatchedDirectories(final String configMessage)
    {
        String directories = "";
        if (configMessage.length() == 0)
        {
            logger.error("setWatchedDirectories: Empty message");
            return Response.status(HTTP_CODE_400).entity(directories).build();
        }

        directoryWatcherConfigService.setConfiguration("directories.watched", configMessage);
        directories = directoryWatcherConfigService.getConfiguration("directories.watched");

        return Response.status(HTTP_CODE_200).entity(directories).build();
    }

    /**
     * The directory watcher config service.
     * 
     * @return directory watcher config service
     */
    public NeoIntConfigurationService getDirectoryWatcherConfigService()
    {
        return directoryWatcherConfigService;
    }

    /**
     * The directory watcher config service.
     * 
     * @param directoryWatcherConfigService
     *            - directory watcher config service
     */
    public void setDirectoryWatcherConfigService(NeoIntConfigurationService directoryWatcherConfigService)
    {
        this.directoryWatcherConfigService = directoryWatcherConfigService;
    }

}
