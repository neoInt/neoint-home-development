/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.index.service;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import com.neoint.server.collect.index.exception.IndexException;
import com.neoint.server.collect.index.model.ContentMetadata;

/**
 * This is the main Index service. This service index the content. The class
 * provides a event based interface which listens on the topic
 * "com/innoplace/intelligence/index". The Event is sent via the OSGi
 * EventAdmin.
 * 
 * @author uwe
 *
 */
public interface IndexService extends EventHandler
{
    /**
     * This method is asynchronous method for indexing the content and metadata
     * of the content.
     * 
     * @param contentMetadata
     *            - metadata of the content for indexing
     * @param content
     *            - content to index
     */
    public boolean addIndexContent(ContentMetadata contentMetadata, String content) throws IndexException;

    /**
     * This method listens on the topic "com/innoplace/intelligence/index" for
     * indexing events. The event must be of type ContentEvent and contains the
     * following properties:
     * <ul>
     * <li>"metadata" of type ContentMetadata</li>
     * <li>"content" of type String</li>
     * </ul>
     * 
     * @param event
     *            - event of type ContentEvent
     */
    public void handleEvent(Event event);

    /**
     * This method deletes the content for the given indexID.
     * 
     * @param indexId
     *            - The index entry which shall be deleted.
     * 
     * @return delete status
     */
    public boolean deleteIndexContent(String indexId);
}
