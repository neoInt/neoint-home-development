/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.index.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This model contains the content which is being indexed.
 * 
 * @author uwe
 *
 */
public class ContentMetadata
{
    private String id = new String();
    private Date createDate = null;
    private Date modifiedDate = null;
    private String title = new String();
    private String description = new String();
    private String type = new String();
    private String referenceUri = new String();
    private Map<String, Object> properties = new HashMap<String, Object>();

    /**
     * This is the create date of the data.
     * 
     * @return createDate
     */
    public Date getCreateDate()
    {
        if (createDate != null)
        {
            return createDate;
        }
        else
        {
            return new Date();
        }
    }

    /**
     * This is the create date of the data.
     * 
     * @param createDate
     *            - creation date
     */
    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    /**
     * This is the modified date of the data.
     * 
     * @return modifiedDate
     */
    public Date getModifiedDate()
    {
        if (modifiedDate != null)
        {
            return modifiedDate;
        }
        else
        {
            return new Date();
        }
    }

    /**
     * This is the modified date of the data.
     * 
     * @param modifiedDate
     *            - modification date
     */
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    /**
     * This is the title of the data.
     * 
     * @return title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * This is the title of the data.
     * 
     * @param title
     *            - title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * This is the description of the data.
     * 
     * @return description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * This is the description of the data.
     * 
     * @param description
     *            - description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * This is the type of the data.
     * 
     * @return type
     */
    public String getType()
    {
        return type;
    }

    /**
     * This is the type of the data.
     * 
     * @param type
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * This are the additional attributes.
     * 
     * @return properties
     */
    public Map<String, ?> getProperties()
    {
        return properties;
    }

    /**
     * This is for adding additional attributes to the index. Properties can
     * contain the following data types
     * <ul>
     * <li>String</li>
     * <li>Double</li>
     * <li>Integer</li>
     * <li>LocalDate</li>
     * </ul>
     * 
     * 
     * @return properties - Map of properties
     */
    public void setProperties(Map<String, Object> properties)
    {
        this.properties = properties;
    }

    /**
     * This is the URI of the original data.
     * 
     * @return URI
     */
    public String getReferenceUri()
    {
        return referenceUri;
    }

    /**
     * This is the URI which leads to the original data.
     * 
     * @param filePath
     *            URI as string
     */
    public void setReferenceUri(String referenceUri)
    {
        this.referenceUri = referenceUri;
    }

    public String getId()
    {
        return id;
    }

    /**
     * This is unqiue Id which will be used for the index. This Id will be used
     * for updating the data in the index.
     * 
     * @param id
     *            - unique Id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * This is unqiue Id which will be used for the index. This Id will be used
     * for updating the data in the index.
     * 
     * @return id - unique Id
     */
    @Override
    public String toString()
    {
        return "Content [createDate=" + getCreateDate() + ", modifiedDate=" + getModifiedDate() + ", title=" + title
                + ", description=" + description + ", type=" + type + ", filePath=" + referenceUri + ", properties="
                + properties.keySet() + "]";
    }

}
