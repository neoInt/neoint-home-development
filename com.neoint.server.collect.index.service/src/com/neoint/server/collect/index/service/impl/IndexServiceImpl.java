/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.index.service.impl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.collect.index.exception.IndexException;
import com.neoint.server.collect.index.model.ContentEvent;
import com.neoint.server.collect.index.model.ContentMetadata;
import com.neoint.server.collect.index.service.IndexService;
import com.neoint.server.core.model.SearchConstants;

/**
 * This is the service implementation of the Indexing service. It receives the
 * index content via OSGi eventadmin or as method call. It calls Elasticsearch
 * for indexing the data.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class IndexServiceImpl implements IndexService
{
    private Client client = null;
    private final Logger logger = LoggerFactory.getLogger(IndexServiceImpl.class.getName());
    private String esClusterName = "neoint";
    private String esHostName = "localhost";
    private int esHostPort = 9300;
    private String indexName = "intelligence";
    private String indexType = "documents";
    private String indexMapping = "";
    private boolean indexExists = false;

    /**
     * This method is called during startup of the service. It creates a
     * Elasticsearch client for later communication.
     * 
     */
    public void startup()
    {
        try
        {
            Settings settings = Settings.settingsBuilder().put("cluster.name", esClusterName).build();
            client = TransportClient.builder().settings(settings).build()
                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(esHostName), esHostPort));
        }
        catch (UnknownHostException e)
        {
            logger.error("Error initializing Elasticsearch Transportclient: {}" + e.getMessage());
        }
    }

    /**
     * This method is called when the service is being stopped. It closes the
     * connection to Elasticsearch.
     */
    public void shutdown()
    {
        if (client != null)
        {
            client.close();
        }
    }

    /**
     * This is the synchronous method for indexing a content.
     * 
     * @param contentMetadata
     *            - the metadata for the content
     * @param content
     *            - the content
     * 
     * @return true if everything went ok and content was indexed.
     */
    @Override
    public boolean addIndexContent(ContentMetadata contentMetadata, String content) throws IndexException
    {
        boolean result = false;

        return result;
    }

    /**
     * The method implementation listens on the topic
     * "com/innoplace/intelligence/index". The Event is sent by the OSGi
     * EventAdmin.
     * 
     * @param event
     *            - The index event
     */
    @Override
    public void handleEvent(Event event)
    {
        if (event instanceof ContentEvent)
        {
            ContentMetadata contentMetadata = (ContentMetadata) event.getProperty(SearchConstants.METADATA);
            String content = (String) event.getProperty(SearchConstants.CONTENT);

            if (client != null)
            {
                try
                {
                    // Check if the index exists
                    if (!indexExists)
                    {
                        IndicesExistsResponse res = client.admin().indices().prepareExists(indexName).execute()
                                .actionGet();
                        if (!res.isExists())
                        {
                            CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices()
                                    .prepareCreate(indexName);
                            createIndexRequestBuilder.addMapping(indexType, indexMapping).execute().actionGet();
                            // IndexResponse response =
                            // client.prepareIndex(indexName, indexType,
                            // null).setSource(indexMapping).execute().actionGet();
                        }
                        indexExists = true;
                    }
                    XContentBuilder builder = XContentFactory.jsonBuilder().startObject()
                            .field(SearchConstants.CREATE_DATE, contentMetadata.getCreateDate())
                            .field(SearchConstants.MODIFIED_DATE, contentMetadata.getModifiedDate())
                            .field(SearchConstants.TITLE, contentMetadata.getTitle())
                            .field(SearchConstants.TYPE, contentMetadata.getType())
                            .field(SearchConstants.REFERENCE_URI, contentMetadata.getReferenceUri())
                            .field(SearchConstants.DESCRIPTION, contentMetadata.getDescription())
                            .field(SearchConstants.CONTENT, content);

                    for (String key : contentMetadata.getProperties().keySet())
                    {
                        builder.field(key, contentMetadata.getProperties().get(key));
                    }
                    builder.endObject();

                    IndexResponse response = client.prepareIndex(indexName, indexType, contentMetadata.getId())
                            .setSource(builder).get();
                    logger.info("Returned index Id: {}", response.getId());
                }
                catch (IOException e)
                {
                    logger.error("Error sending content to Elasticsearch: " + e.getMessage());
                }
            }
            else
            {
                logger.error("Error sending content to Elasticsearch. Client is null.");
            }

        }
    }

    /**
     * This method deletes the content for the given indexID.
     * 
     * @param indexId
     *            - The index entry which shall be deleted.
     * 
     * @return delete status
     */
    @Override
    public boolean deleteIndexContent(String indexId)
    {
        DeleteResponse response = client.prepareDelete(indexName, indexType, indexId).get();
        logger.info("Deleted index Id: {}", response.getId());

        return true;
    }

    /**
     * The Elasticsearch cluster name configuration
     * 
     * @return Elasticsearch cluster name
     */
    public String getEsClusterName()
    {
        return esClusterName;
    }

    /**
     * The Elasticsearch cluster name configuration
     * 
     * @param esClusterName
     *            - Elasticsearch cluster name
     */
    public void setEsClusterName(String esClusterName)
    {
        this.esClusterName = esClusterName;
    }

    /**
     * The Elasticsearch host name configuration
     * 
     * @return host name
     */
    public String getEsHostName()
    {
        return esHostName;
    }

    /**
     * The Elasticsearch host name configuration
     * 
     * @param esHostName
     *            - Host name
     */
    public void setEsHostName(String esHostName)
    {
        this.esHostName = esHostName;
    }

    /**
     * The Elasticsearch host port configuration
     * 
     * @return Host port
     */
    public int getEsHostPort()
    {
        return esHostPort;
    }

    /**
     * The Elasticsearch host port configuration
     * 
     * @param esHostPort
     *            - Host port
     */
    public void setEsHostPort(int esHostPort)
    {
        this.esHostPort = esHostPort;
    }

    /**
     * The Elasticsearch index name
     * 
     * @return indexName
     */
    public String getIndexName()
    {
        return indexName;
    }

    /**
     * The Elasticsearch index name
     * 
     * @param indexName
     *            - index name
     */
    public void setIndexName(String indexName)
    {
        this.indexName = indexName;
    }

    /**
     * The Elasticsearch index type
     * 
     * @return indexType
     */
    public String getIndexType()
    {
        return indexType;
    }

    /**
     * The Elasticsearch index type
     * 
     * @param indexType
     *            - index type
     */
    public void setIndexType(String indexType)
    {
        this.indexType = indexType;
    }

    /**
     * @return the indexMapping
     */
    public String getIndexMapping()
    {
        return indexMapping;
    }

    /**
     * @param indexMapping
     *            the indexMapping to set
     */
    public void setIndexMapping(String indexMapping)
    {
        this.indexMapping = indexMapping;
    }
}
