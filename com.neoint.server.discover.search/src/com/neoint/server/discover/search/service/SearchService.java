/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.discover.search.service;

import com.neoint.server.discover.search.model.SearchResult;

/**
 * This Interface is the main interface for searching data.
 * 
 * @author uwe
 *
 */
public interface SearchService
{

    /**
     * This operation provides a full text search with pagination and filtering.
     * 
     * @param query
     *            - query as full text
     * 
     * @param start
     *            - start pagination
     * 
     * @param size
     *            - number of results
     * 
     * @param filterField
     *            - field for filtering
     * 
     * @param filterTerm
     *            - term for which should be filtered
     * 
     * @return Found result
     */
    public SearchResult find(String query, int start, int size, String filterField, String filterTerm);

    /**
     * This operation provides the metadata for the given
     * 
     * @param id
     *            - id for which all metadata shall be retrieved
     * 
     * 
     * @return Found result as JSON string
     */
    public String getAllMetadata(String id);

    /**
     * This operation returns all mapping fields from the search engine
     * 
     * @return Mappings result as JSON string
     */
    public String getAllMappings();
}
