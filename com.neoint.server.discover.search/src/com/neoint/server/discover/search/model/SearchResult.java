/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.discover.search.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is the model for the search result. It contains a list of entries.
 * 
 * @author uwe
 *
 */
public class SearchResult
{
    private long hits = 0;
    private List<SearchResultEntry> entries = new ArrayList<SearchResultEntry>();
    private long searchDuration;

    private Map<String, Long> contentTypeCounts = new HashMap<String, Long>();

    /**
     * The result hits
     * 
     * @return - number of hits
     */
    public long getHits()
    {
        return hits;
    }

    /**
     * The result hits
     * 
     * @param hits
     *            - number of hits
     */
    public void setHits(long hits)
    {
        this.hits = hits;
    }

    /**
     * The result entries
     * 
     * @return entries
     */
    public List<SearchResultEntry> getEntries()
    {
        return entries;
    }

    /**
     * The result entries
     * 
     * @param entries
     *            - entries
     */
    public void setEntries(List<SearchResultEntry> entries)
    {
        this.entries = entries;
    }

    /**
     * Time in milliseconds how long the query took
     * 
     * @return duration in milliseconds
     */
    public long getSearchDuration()
    {
        return searchDuration;
    }

    /**
     * Time in milliseconds how long the query took
     * 
     * @param searchDuration
     *            - time in milliseconds
     */
    public void setSearchDuration(long searchDuration)
    {
        this.searchDuration = searchDuration;
    }

    /**
     * Adds an entry to the search result.
     * 
     * @param entry
     *            - entry which shall be added
     */
    public void addEntry(SearchResultEntry entry)
    {
        entries.add(entry);
    }

    /**
     * @return the contentTypeCounts
     */
    public Map<String, Long> getContentTypeCounts()
    {
        return contentTypeCounts;
    }

    /**
     * @param contentTypeCounts
     *            the contentTypeCounts to set
     */
    public void setContentTypeCounts(Map<String, Long> contentTypeCounts)
    {
        this.contentTypeCounts = contentTypeCounts;
    }
}
