/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.discover.resource.service;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

import com.neoint.server.core.exception.InvalidProtocolException;

/**
 * This Service resolves the content depending on the URI and returns the
 * content.
 * 
 * @author uwe
 *
 */
public interface ResourceService
{
    /**
     * This method resolves the content depending on the URI and returns it as
     * stream. The streamer has to be given as parameter.
     * 
     * @param uri
     * 
     * @param outputStream
     *
     */
    public void getContent(URL uri, OutputStream outputStream) throws InvalidProtocolException, IOException;

    /**
     * This method resolves the content depending on the URI and returns it as
     * thumbnail. The streamer has to be given as parameter.
     * 
     * @param uri
     * 
     * @param OutputStream
     * @throws IOException
     *
     */
    public void getThumbnail(URL fileUrl, OutputStream outputStream, int width, int height)
            throws InvalidProtocolException, IOException;

    /**
     * This operation provides the metadata for the given
     * 
     * @param id
     *            - id for which all metadata shall be retrieved
     * 
     * 
     * @return Found result as JSON string
     */
    public String getAllMetadata(String id);
}
