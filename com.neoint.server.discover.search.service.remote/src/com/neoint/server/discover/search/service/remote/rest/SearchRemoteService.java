/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.discover.search.service.remote.rest;

import java.util.concurrent.TimeUnit;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.jcabi.aspects.Loggable;
import com.neoint.server.discover.search.model.SearchResult;
import com.neoint.server.discover.search.service.SearchService;

/**
 * This is the remote RESTful service for the Search Service.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class SearchRemoteService
{
    private static final int HTTP_CODE_200 = 200;
    private static final int HTTP_CODE_500 = 500;
    private static final int HTTP_CODE_400 = 400;
    private final Logger logger = LoggerFactory.getLogger(SearchRemoteService.class.getName());
    private SearchService searchService = null;

    /**
     * This operation does the search.
     * 
     * @param searchMessage
     *            - JSON {"query_string":"query string", "size": size, "from":
     *            from,"filter_field":"filterField","filter_term", "filterTerm"}
     *
     *            filter_field and filter_term are optional
     *
     * @return search result as JSON
     */
    @POST
    @Produces("application/json; charset=UTF-8")
    public final Response search(final String searchMessage)
    {
        final JSONObject parseMessageJson;
        String result = "";
        if (searchMessage.length() == 0)
        {
            logger.error("search: Empty message");
            return Response.status(HTTP_CODE_400).entity(result).build();
        }
        try
        {
            parseMessageJson = new JSONObject(searchMessage);
        }
        catch (JSONException ex)
        {
            logger.error("search: JSON Exeption for {} message {}", ex.getMessage(), searchMessage);
            return Response.status(HTTP_CODE_400).entity(result).build();
        }

        String filterField = parseMessageJson.optString("filter_field", null);
        String filterTerm = parseMessageJson.optString("filter_term", null);

        try
        {
            SearchResult searchResult = searchService.find(parseMessageJson.getString("query_string"), parseMessageJson
                    .getInt("from"), parseMessageJson.getInt("size"), filterField, filterTerm);
            Gson gson = new Gson();
            result = gson.toJson(searchResult);
        }
        catch (JSONException e)
        {
            logger.error("search: JSON Exeption for {} message {}", e.getMessage(), searchMessage);
            return Response.status(HTTP_CODE_500).entity(result.toString()).build();
        }

        return Response.status(HTTP_CODE_200).entity(result).build();
    }

    /**
     * This operation loads all index fields.
     * 
     * @return mappings as JSON
     */
    @GET
    @Path("/mappings")
    @Produces("application/json; charset=UTF-8")
    public final Response getContent()
    {
        String result = "";

        result = searchService.getAllMappings();

        return Response.status(HTTP_CODE_200).entity(result.toString()).build();
    }

    /**
     * The search service.
     * 
     * @return search service
     */
    public SearchService getSearchService()
    {
        return searchService;
    }

    /**
     * The search service.
     * 
     * @param searchService
     *            - search service
     */
    public void setSearchService(SearchService searchService)
    {
        this.searchService = searchService;
    }
}
