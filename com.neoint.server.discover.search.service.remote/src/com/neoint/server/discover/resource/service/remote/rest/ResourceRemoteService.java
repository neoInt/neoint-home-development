/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.discover.resource.service.remote.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.core.exception.InvalidProtocolException;
import com.neoint.server.discover.resource.service.ResourceService;

/**
 * This is the remote RESTful service for the preview the searched content.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class ResourceRemoteService
{

    private static final int HTTP_CODE_200 = 200;
    private static final int HTTP_CODE_500 = 500;
    private static final int HTTP_CODE_400 = 400;
    private final Logger logger = LoggerFactory.getLogger(ResourceRemoteService.class.getName());
    private ResourceService resourceService = null;

    /**
     * This operation loads the content depending on the URI and returns as
     * stream.
     * 
     * @param id
     *            as URI
     *
     * @return product as binary stream
     */
    @GET
    @Path("/download")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public final Response getContent(@QueryParam("uri") final String contentId)
    {
        String fileName = "";
        final URL fileUrl;
        if (contentId.length() == 0)
        {
            logger.error("getContent: Empty message");
            return null;
        }
        try
        {

            fileUrl = new URL(contentId);
            fileName = URLEncoder.encode(fileUrl.getFile()
                    .substring(fileUrl.getFile().lastIndexOf("/") + 1, fileUrl.getFile().length()), "UTF-8");
            fileName = URLDecoder.decode(fileName, "ISO8859_1");
        }
        catch (UnsupportedEncodingException | MalformedURLException ex)
        {
            logger.error("getContent: Exeption for {} message {}", ex.getMessage(), contentId);
            return Response.status(HTTP_CODE_500).entity("").build();
        }

        StreamingOutput stream = new StreamingOutput()
        {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException
            {
                try
                {
                    resourceService.getContent(fileUrl, os);
                }
                catch (InvalidProtocolException e)
                {
                    logger.error("getContent: Parsing Excpetion {} for message {} ", e.getMessage(), contentId);
                }
            }
        };

        return Response.ok(stream).header("Content-disposition", "attachment; filename=" + fileName).build();
    }

    /**
     * This operation loads the preview depending on the URI. It creates a
     * thumbnail and returns as stream.
     * 
     * @param id
     *            as URI
     *
     * @return thumbnail as 256x256px stream
     */
    @GET
    @Path("/thumbnail")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public final Response getThumbnail(@QueryParam("uri") final String contentId)
    {
        String fileName = "";
        final URL fileUrl;
        if (contentId == null || contentId.length() == 0)
        {
            logger.error("getThumbnail: Empty message");
            return null;
        }
        try
        {

            fileUrl = new URL(contentId);
            fileName = URLEncoder.encode(fileUrl.getFile()
                    .substring(fileUrl.getFile().lastIndexOf("/") + 1, fileUrl.getFile().length()), "UTF-8");
            fileName = URLDecoder.decode(fileName, "ISO8859_1");
        }
        catch (UnsupportedEncodingException | MalformedURLException ex)
        {
            logger.error("getContent: Exeption for {} message {}", ex.getMessage(), contentId);
            return null;
        }

        StreamingOutput stream = new StreamingOutput()
        {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException
            {
                try
                {
                    resourceService.getThumbnail(fileUrl, os, 128, 128);
                }
                catch (InvalidProtocolException e)
                {
                    logger.error("getContent: Parsing Excpetion {} for message {} ", e.getMessage(), contentId);
                }
            }
        };

        return Response.ok(stream).header("Content-disposition", "attachment; filename=" + fileName).build();
    }

    /**
     * This operation loads the metadata for the given id.
     * 
     * @param id
     *            as URI
     *
     * @return metadata as JSON
     */
    @GET
    @Path("/metadata")
    @Produces("application/json; charset=UTF-8")
    public final Response getMetadata(@QueryParam("id") final String contentId)
    {
        String result = "";
        if (contentId.length() == 0)
        {
            logger.error("getMetadata: Empty message");
            return null;
        }
        result = resourceService.getAllMetadata(contentId);
        return Response.status(HTTP_CODE_200).entity(result).build();
    }

    /**
     * @return the resourceService
     */
    public ResourceService getResourceService()
    {
        return resourceService;
    }

    /**
     * @param resourceService
     *            the resourceService to set
     */
    public void setResourceService(ResourceService resourceService)
    {
        this.resourceService = resourceService;
    }
}
