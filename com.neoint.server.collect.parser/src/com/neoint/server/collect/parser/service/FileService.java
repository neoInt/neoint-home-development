/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.parser.service;

import java.io.File;
import java.util.List;

/**
 * This is the interface of the File Service. It provides functionality for
 * directory listing and file lists.
 * 
 * @author uwe
 *
 */
public interface FileService
{
    /**
     * This method returns a list of files.
     * 
     * @param directoryName
     *            - Directory which shall be searched
     * 
     * @param recursive
     *            - If search is recursive
     * 
     * @param excludedFiles
     *            - Which files shall be excluded. The file suffix e.g. exe,dll
     *            etc has to be given.
     * 
     * @return List of files
     */
    public List<File> getFiles(String directoryName, boolean recursive, List<String> excludedFiles);

    /**
     * This method returns a list of directories.
     * 
     * @param directoryName
     *            - Directory which shall be searched
     * @param recursive
     *            - If search is recursive
     * 
     * @return List of directory paths
     */
    public List<String> getDirectories(String directoryName, boolean recursive);
}
