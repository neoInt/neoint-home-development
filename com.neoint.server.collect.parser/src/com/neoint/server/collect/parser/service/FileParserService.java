/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.parser.service;

import java.io.Writer;
import java.net.URL;

import com.neoint.server.core.exception.ParserException;

/**
 * This service parses files and sends the metadata and content as event. The
 * event will be sent to the topic "com/innoplace/intelligence/index".
 * 
 * @author uwe
 * 
 */
public interface FileParserService
{

    /**
     * This method parses all files from the given directory. If recursive is
     * true all subdirectories will be scanned to. The parsed content will be
     * sent as index events.
     * 
     * @param path
     *            - path to a dircetory on which the service has server has
     *            access.
     * 
     * @param recursive
     *            - true if the dirctory with all subdirectories shall be
     *            scanned.
     */
    public void parseFilesFromDirectoryAndIndex(String path, boolean recursive) throws ParserException;

    /**
     * This method parses all files from the given directory. If recursive is
     * true all subdirectories will be scanned to. The parsed content will be
     * sent as index events. It can be used for streaming the progress.
     * 
     * @param path
     *            - path to a dircetory on which the service has server has
     *            access.
     * 
     * @param recursive
     *            - true if the dirctory with all subdirectories shall be
     *            scanned.
     *
     * @param writer
     *            - a writer where the output progress can be written. The
     *            output will be written JSON String {"progress":
     *            "value as percent"
     *            ,"status":"started/running/finished","filesParsed": parsed
     *            number ,"filesTotal":total number}
     */
    public void parseFilesFromDirectoryAndIndex(String path, boolean recursive, Writer writer) throws ParserException;

    /**
     * This method parses a file from the given URL. The supported URL protocols
     * are:
     * <ul>
     * <li>HTTP(s) (e.g. http://username:password@hostname:port/path/to/file)
     * </li>
     * <li>FTP (ftp://username:password@hostname/path/to/file)</li>
     * <li>File (file:///path/to/mounted/file)</li>
     * </ul>
     * 
     * The parsed content will be returned.
     * 
     * @param url
     *            - The URL of the file which shall be indexed.
     * 
     * @return returns the content as JSON
     */
    public String parseFile(URL url) throws ParserException;

    /**
     * This method parses a file from the given URL. The supported URL protocols
     * are:
     * <ul>
     * <li>HTTP(s) (e.g. http://username:password@hostname:port/path/to/file)
     * </li>
     * <li>FTP (ftp://username:password@hostname/path/to/file)</li>
     * <li>File (file:///path/to/mounted/file)</li>
     * </ul>
     * 
     * The parsed content will be sent to the index.
     * 
     * @param url
     *            - The URL of the file which shall be indexed.
     * 
     * @return returns true if parsing worked and event could be sent.
     */
    public boolean parseFileAndIndex(URL url) throws ParserException;

    /**
     * This contains the file extensions which shall be excluded during parsing.
     * 
     * @return comma separated of file extensions
     */
    public String getParserFileExtContentExclude();

    /**
     * This contains the file extensions which shall be excluded during parsing.
     * 
     * @param parserFileExtContentExclude
     *            - comma separated String of file extensions
     */
    public void setParserFileExtContentExclude(String parserFileExtContentExclude);
}
