/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.core.service;

import java.util.Dictionary;

/**
 * Interface for the changing the configuration of the File Parser Service.
 * 
 * @author uwe
 *
 */
public interface NeoIntConfigurationService
{
    /**
     * Returns the configuration for the given property.
     * 
     * @param property
     *            - Property which shall be loaded
     * 
     * @return configuration string of the property
     */
    public String getConfiguration(String property);

    /**
     * Sets the value for the given property.
     * 
     * @param property
     *            - Property
     * 
     * @param value
     *            - Value
     * @return
     */
    public boolean setConfiguration(String property, String value);

    /**
     * Sets the values for the the properties.
     * 
     * @param properties
     *            - Properties
     * 
     * @return
     */
    public boolean setConfigurationProperties(Dictionary<String, Object> properties);

    /**
     * Returns all configuration properties as key values.
     * 
     * @return Configuration properties as key values.
     */
    public Dictionary<String, Object> getConfigProperties();

    /**
     * Deletes the property from the configuration.
     * 
     * @param property
     *            - Property which shall be deleted
     * @return
     */
    public boolean deleteConfiguration(String property);
}
