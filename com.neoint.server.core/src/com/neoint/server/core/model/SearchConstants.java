/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.core.model;

public class SearchConstants
{
    public static String MODIFIED_DATE = "modifiedDate";
    public static String TITLE = "title";
    public static String DESCRIPTION = "description";
    public static String CREATE_DATE = "createDate";
    public static String TYPE = "type";
    public static String REFERENCE_URI = "referenceUri";
    public static String CONTENT = "content";
    public static String CONTENT_TYPE = "Content-Type";
    public static String CONTENT_TYPE_RAW = "Content-Type.raw";
    public static String METADATA = "metadata";
}
