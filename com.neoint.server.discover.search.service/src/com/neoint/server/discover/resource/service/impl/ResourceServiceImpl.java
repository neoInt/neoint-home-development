/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.discover.resource.service.impl;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.core.exception.InvalidProtocolException;
import com.neoint.server.core.model.ErrorCodes;
import com.neoint.server.discover.resource.service.ResourceService;
import com.neoint.server.discover.search.service.SearchService;

/**
 * This Service resolves the content depending on the URI and returns the
 * content.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class ResourceServiceImpl implements ResourceService
{
    private final Logger logger = LoggerFactory.getLogger(ResourceServiceImpl.class.getName());
    private SearchService searchService = null;

    /**
     * This method resolves the content depending on the URI and returns it as
     * stream. The streamer has to be given as parameter.
     * 
     * @param uri
     * 
     * @param OutputStream
     * @throws IOException
     *
     */
    public void getContent(URL fileUrl, OutputStream outputStream) throws InvalidProtocolException, IOException
    {
        if (fileUrl.getProtocol().startsWith("file"))
        {
            logger.info("Reading file {}", fileUrl.getFile());
            File file = new File(fileUrl.getPath());
            FileInputStream is = new FileInputStream(file);
            int len = 0;
            logger.info("File size {}", file.length());
            byte[] buffer = new byte[4096];
            while ((len = is.read(buffer)) != -1)
            {
                outputStream.write(buffer, 0, len);
            }
            outputStream.flush();
            outputStream.close();
            is.close();
            logger.info("Finished");

        }
        else
        {
            throw new InvalidProtocolException("Protocol " + fileUrl.getProtocol() + " not supported.",
                    ErrorCodes.GENERAL_ERROR);
        }
    }

    /**
     * This method resolves the content depending on the URI and returns it as
     * thumbnail. The streamer has to be given as parameter.
     * 
     * @param uri
     * 
     * @param OutputStream
     * @throws IOException
     *
     */
    public void getThumbnail(URL fileUrl, OutputStream outputStream, int width, int height)
            throws InvalidProtocolException, IOException
    {
        if (fileUrl.getProtocol().startsWith("file"))
        {
            logger.info("Reading file {}", fileUrl.getFile());

            BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            img.createGraphics().drawImage(ImageIO.read(new File(fileUrl.getPath()))
                    .getScaledInstance(width, height, Image.SCALE_SMOOTH), 0, 0, null);

            BufferedImage scaled = scale(img, width, height);
            ImageIO.write(scaled, fileUrl.getPath().substring(fileUrl.getPath().lastIndexOf(".") + 1), outputStream);

            outputStream.flush();
            outputStream.close();
            logger.info("Finished");
        }
        else
        {
            throw new InvalidProtocolException("Protocol " + fileUrl.getProtocol() + " not supported.",
                    ErrorCodes.GENERAL_ERROR);
        }
    }

    /**
     * This operation provides the metadata for the given
     * 
     * @param id
     *            - id for which all metadata shall be retrieved
     * 
     * 
     * @return Found result as JSON string
     */
    @Override
    public String getAllMetadata(String id)
    {
        String result = "";

        result = searchService.getAllMetadata(id);
        return result;
    }

    private BufferedImage scale(BufferedImage source, int width, int height)
    {
        BufferedImage bi = getCompatibleImage(width, height);
        Graphics2D g2d = bi.createGraphics();
        double xScale = (double) width / source.getWidth();
        double yScale = (double) height / source.getHeight();
        AffineTransform at = AffineTransform.getScaleInstance(xScale, yScale);
        g2d.drawRenderedImage(source, at);
        g2d.dispose();
        return bi;
    }

    private BufferedImage getCompatibleImage(int w, int h)
    {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        GraphicsConfiguration gc = gd.getDefaultConfiguration();
        BufferedImage image = gc.createCompatibleImage(w, h);
        return image;
    }

    /**
     * @return the searchService
     */
    public SearchService getSearchService()
    {
        return searchService;
    }

    /**
     * @param searchService
     *            the searchService to set
     */
    public void setSearchService(SearchService searchService)
    {
        this.searchService = searchService;
    }
}
