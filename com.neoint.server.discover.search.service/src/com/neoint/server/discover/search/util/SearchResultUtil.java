/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.discover.search.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides multiple utility methods for the search result.
 * 
 * @author uwe
 *
 */
public class SearchResultUtil
{
    private final static Logger logger = LoggerFactory.getLogger(SearchResultUtil.class.getName());

    /**
     * This method searches the content for the corresponding keywords and
     * returns a abstract which includes the content. Result will be like this:
     * ... bla bla keyword blub blubb ... blubb blubb keyword bla bla ...
     * 
     * If nothing was found the original content is returned.
     * 
     * @param content
     *            - Content which should be parsed for the keywords.
     * 
     * @param keywords
     *            - Content which should be parsed for the keywords.
     * 
     * @param contentLengthBeforeKeyword
     *            - Number of characters in front of the keyword
     * 
     * @param contentLengthAfterKeyword
     *            - Number of characters after the keyword
     * 
     * @return sub content
     */
    public static String getSubContent(String content, List<String> keywords, int contentLengthBeforeKeyword,
            int contentLengthAfterKeyword, int numberOfHighlights)
    {
        String result = "";
        int highlightsPerKeyword = numberOfHighlights / keywords.size();
        int i = 0;
        if (content.length() > 2)
        {
            for (String keyword : keywords)
            {
                int indexOffset = 0;
                keyword = keyword.trim();
                while (content.toLowerCase().indexOf(keyword.toLowerCase(), indexOffset) != -1)
                {
                    int beginIndex = content.toLowerCase().indexOf(keyword.toLowerCase(), indexOffset);
                    if ((beginIndex - contentLengthBeforeKeyword) < 0)
                    {
                        beginIndex = 0;
                    }
                    else
                    {
                        beginIndex = beginIndex - contentLengthBeforeKeyword;
                    }
                    int endIndex = content.toLowerCase().indexOf(keyword.toLowerCase(), indexOffset) + keyword.length();
                    if ((endIndex + contentLengthAfterKeyword) > content.length())
                    {
                        endIndex = content.length();
                    }
                    else
                    {
                        endIndex = endIndex + contentLengthAfterKeyword;
                    }

                    result = result + " ... " + content.substring(beginIndex, endIndex);
                    indexOffset = endIndex;
                    i++;
                    if (i == highlightsPerKeyword)
                    {
                        break;
                    }
                }
                i = 0;
            }
        }
        if (result.isEmpty())
        {
            result = content;
        }
        logger.debug("subcontent: {}", result);
        return result;
    }
}
