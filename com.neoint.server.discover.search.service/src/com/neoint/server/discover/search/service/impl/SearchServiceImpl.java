/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.discover.search.service.impl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.jcabi.aspects.Loggable;
import com.neoint.server.core.model.SearchConstants;
import com.neoint.server.discover.search.model.SearchResult;
import com.neoint.server.discover.search.model.SearchResultEntry;
import com.neoint.server.discover.search.service.SearchService;
import com.neoint.server.discover.search.util.SearchResultUtil;

/**
 * This is the implementation of the Search Service.
 * 
 * @author uwe
 *
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class SearchServiceImpl implements SearchService
{

    private Client client = null;
    private final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class.getName());
    private String esClusterName = "neoint";
    private String esHostName = "localhost";
    private int esHostPort = 9300;
    private String indexName = "intelligence";
    private String indexType = "documents";
    private int contentLengthBeforeKeyword = 50;
    private int contentLengthAfterKeyword = 50;
    private int numberOfHighlights = 10;

    /**
     * This method is called during startup of the service. It creates a
     * Elasticsearch client for later communication.
     * 
     */
    public void startup()
    {
        try
        {
            Settings settings = Settings.settingsBuilder().put("cluster.name", esClusterName).build();
            client = TransportClient.builder().settings(settings).build()
                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(esHostName), esHostPort));
        }
        catch (UnknownHostException e)
        {
            logger.error("Error initializing Elasticsearch Transportclient: {}" + e.getMessage());
        }
    }

    /**
     * This method is called when the service is being stopped. It closes the
     * connection to Elasticsearch.
     */
    public void shutdown()
    {
        if (client != null)
        {
            client.close();
        }
    }

    /**
     * This operation provides a full text search with pagination and filtering.
     * 
     * @param query
     *            - query as full text
     * 
     * @param start
     *            - start pagination
     * 
     * @param size
     *            - number of results
     * 
     * @param filterField
     *            - field for filtering (null for disable filtering)
     * 
     * @param filterTerm
     *            - term for which should be filtered (null for disable
     *            filtering)
     * 
     * @return Found result
     */
    @Override
    public SearchResult find(String query, int start, int size, String filterField, String filterTerm)
    {
        SearchResult result = new SearchResult();
        List<String> keywords = new ArrayList<String>();
        String queryKeywords = query.replaceAll("/\\[(.*?)\\]", " ");
        queryKeywords = queryKeywords.replaceAll("AND", "");
        queryKeywords = queryKeywords.replaceAll("OR", "");
        query = query.replaceAll("\"", "\\\"");
        logger.info("query keywords: {}", queryKeywords);
        keywords = Arrays.asList(queryKeywords.split(" "));

        QueryBuilder queryBuilder = QueryBuilders.queryStringQuery(query);

        if (filterField != null && filterTerm != null)
        {
            queryBuilder = QueryBuilders.boolQuery().must(QueryBuilders.wildcardQuery(filterField, filterTerm))
                    .must(queryBuilder);

        }
        logger.info("Query: {}", queryBuilder.toString());
        SearchRequestBuilder request = client.prepareSearch(indexName).setTypes(indexType)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setQuery(queryBuilder) // Query
                .setFrom(start).setSize(size)
                .addAggregation(AggregationBuilders.terms("types").field(SearchConstants.CONTENT_TYPE_RAW))
                .setExplain(true);

        // logger.info(request.toString());

        SearchResponse response = request.execute().actionGet();
        Terms agg = response.getAggregations().get("types");

        // logger.info(response.toString());
        result.setHits(response.getHits().getTotalHits());
        result.setSearchDuration(response.getTookInMillis());

        for (SearchHit hit : response.getHits().getHits())
        {
            SearchResultEntry entry = new SearchResultEntry();
            // Date format from Elasticsearch: 2016-03-31T07:14:51.000Z
            try
            {
                entry.setId(hit.getId());
                String createDate = (String) hit.getSource().get(SearchConstants.CREATE_DATE);
                String modifiedDate = (String) hit.getSource().get(SearchConstants.MODIFIED_DATE);
                if (createDate != null)
                {
                    entry.setCreateDate(Date.from(LocalDateTime
                            .parse((String) hit.getSource().get(SearchConstants.CREATE_DATE), DateTimeFormatter
                                    .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))
                            .atZone(ZoneId.systemDefault()).toInstant()));
                }
                if (modifiedDate != null)
                {
                    entry.setModifiedDate(Date.from(LocalDateTime
                            .parse(modifiedDate, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))
                            .atZone(ZoneId.systemDefault()).toInstant()));
                }
                entry.setTitle((String) hit.getSource().get(SearchConstants.TITLE));
                entry.setDescription((String) hit.getSource().get(SearchConstants.DESCRIPTION));
                entry.setReferenceUri((String) hit.getSource().get(SearchConstants.REFERENCE_URI));
                entry.setType((String) hit.getSource().get(SearchConstants.TYPE));
                entry.setContent(SearchResultUtil.getSubContent((String) hit.getSource()
                        .get(SearchConstants.CONTENT), keywords, contentLengthBeforeKeyword, contentLengthAfterKeyword, numberOfHighlights));
                entry.setContentType((String) hit.getSource().get(SearchConstants.CONTENT_TYPE));

                Set<String> keys = hit.getSource().keySet();
                for (String key : keys)
                {
                    entry.addProperty(key, hit.getSource().get(key));
                }
            }
            catch (Exception e)
            {
                logger.error("Exception for hit {} ", hit.getSource());
            }
            result.addEntry(entry);
        }

        Map<String, Long> contentTypeCount = new HashMap<String, Long>();

        // For each entry
        for (Terms.Bucket entry : agg.getBuckets())
        {
            String key = (String) entry.getKey(); // bucket key
            long docCount = entry.getDocCount(); // Doc count
            logger.info("key [{}], doc_count [{}]", key, docCount);
            contentTypeCount.put(key, docCount);
        }
        result.setContentTypeCounts(contentTypeCount);

        return result;
    }

    /**
     * This operation provides the metadata for the given
     * 
     * @param id
     *            - id for which all metadata shall be retrieved
     * 
     * 
     * @return Found result as JSON string
     */
    @Override
    public String getAllMetadata(String id)
    {
        String result = "";

        GetResponse response = client.prepareGet(indexName, indexType, id).get();
        result = response.getSourceAsString();
        return result;
    }

    /**
     * This operation returns all mapping fields from the search engine
     * 
     * @return Mappings result as JSON string
     */
    @Override
    public String getAllMappings()
    {
        String result = "";

        List<String> typeNames = new ArrayList<String>();
        GetMappingsResponse mappingsResponse = client.admin().indices().prepareGetMappings(indexName).execute()
                .actionGet();

        try
        {
            ImmutableOpenMap<String, ImmutableOpenMap<String, MappingMetaData>> mappings = mappingsResponse.mappings();
            ImmutableOpenMap<String, MappingMetaData> indexMappings = mappings.get(indexName);
            for (Iterator<String> iterator = indexMappings.keysIt(); iterator.hasNext();)
            {
                String typeName = iterator.next();
                logger.debug("typeName {} ", typeName);
                MappingMetaData mappingMetadata = indexMappings.get(typeName);

                // Get mapping content for the type
                Map<String, Object> source = mappingMetadata.sourceAsMap();
                Map<String, Object> properties = (Map<String, Object>) source.get("properties");
                if (properties != null)
                {
                    // Iterate over mapping properties for the type
                    for (String propertyName : properties.keySet())
                    {
                        if (!propertyName.startsWith("Unknown"))
                        {
                            typeNames.add(propertyName);
                        }
                        Map<String, String> property = (Map<String, String>) properties.get(propertyName);
                        String type = property.get("type");
                        logger.debug("typeName {}, propertyName {}, type {} ", typeName, propertyName, type);
                    }
                }
            }
            Gson gson = new Gson();
            result = gson.toJson(typeNames);
        }
        catch (IOException ex)
        {
            logger.error("IOException for getMappings {} ", ex.getStackTrace());
        }
        return result;
    }

    /**
     * The Elasticsearch cluster name configuration
     * 
     * @return Elasticsearch cluster name
     */
    public String getEsClusterName()
    {
        return esClusterName;
    }

    /**
     * The Elasticsearch cluster name configuration
     * 
     * @param esClusterName
     *            - Elasticsearch cluster name
     */
    public void setEsClusterName(String esClusterName)
    {
        this.esClusterName = esClusterName;
    }

    /**
     * The Elasticsearch host name configuration
     * 
     * @return host name
     */
    public String getEsHostName()
    {
        return esHostName;
    }

    /**
     * The Elasticsearch host name configuration
     * 
     * @param esHostName
     *            - Host name
     */
    public void setEsHostName(String esHostName)
    {
        this.esHostName = esHostName;
    }

    /**
     * The Elasticsearch host port configuration
     * 
     * @return Host port
     */
    public int getEsHostPort()
    {
        return esHostPort;
    }

    /**
     * The Elasticsearch host port configuration
     * 
     * @param esHostPort
     *            - Host port
     */
    public void setEsHostPort(int esHostPort)
    {
        this.esHostPort = esHostPort;
    }

    /**
     * The Elasticsearch index name
     * 
     * @return indexName
     */
    public String getIndexName()
    {
        return indexName;
    }

    /**
     * The Elasticsearch index name
     * 
     * @param indexName
     *            - index name
     */
    public void setIndexName(String indexName)
    {
        this.indexName = indexName;
    }

    /**
     * The Elasticsearch index type
     * 
     * @return indexType
     */
    public String getIndexType()
    {
        return indexType;
    }

    /**
     * The Elasticsearch index type
     * 
     * @param indexType
     *            - index type
     */
    public void setIndexType(String indexType)
    {
        this.indexType = indexType;
    }

    /**
     * @return the contentBeforeKeyword
     */
    public int getContentLengthBeforeKeyword()
    {
        return contentLengthBeforeKeyword;
    }

    /**
     * @param contentLengthBeforeKeyword
     *            the contentLengthBeforeKeyword to set
     */
    public void setContentLengthBeforeKeyword(int contentLengthBeforeKeyword)
    {
        this.contentLengthBeforeKeyword = contentLengthBeforeKeyword;
    }

    /**
     * @return the contentAfterKeyword
     */
    public int getContentLengthAfterKeyword()
    {
        return contentLengthAfterKeyword;
    }

    /**
     * @param contentLengthAfterKeyword
     *            the contentLengthAfterKeyword to set
     */
    public void setContentLengthAfterKeyword(int contentLengthAfterKeyword)
    {
        this.contentLengthAfterKeyword = contentLengthAfterKeyword;
    }

    /**
     * @return the numberOfHighlights
     */
    public int getNumberOfHighlights()
    {
        return numberOfHighlights;
    }

    /**
     * @param numberOfHighlights
     *            the numberOfHighlights to set
     */
    public void setNumberOfHighlights(int numberOfHighlights)
    {
        this.numberOfHighlights = numberOfHighlights;
    }
}
