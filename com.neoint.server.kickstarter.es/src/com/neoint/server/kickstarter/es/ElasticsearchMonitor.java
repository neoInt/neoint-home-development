/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.kickstarter.es;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class monitors the elasticsearch process. If it crashes, it restarts the
 * process.
 * 
 * @author uwe
 *
 */
public class ElasticsearchMonitor extends Thread
{
    private final Logger logger = LoggerFactory.getLogger(ElasticsearchMonitor.class.getName());
    private ElasticsearchProcess esProcess;
    private final String processExecutable = System.getProperty("karaf.base") + "/search-engine/bin/elasticsearch";
    private boolean stopping = false;

    public ElasticsearchMonitor()
    {
        super();
        try
        {
            ProcessBuilder pb = new ProcessBuilder(processExecutable);
            esProcess = new ElasticsearchProcess(pb.start());
            esProcess.start();
        }
        catch (IOException e)
        {
            logger.error("Cannot start Elasticsearch process: {}" + e.getMessage());
        }
    }

    @Override
    public void run()
    {
        try
        {
            while (!stopping)
            {
                if (esProcess.isComplete())
                {
                    ProcessBuilder pb = new ProcessBuilder(processExecutable);
                    try
                    {
                        esProcess = new ElasticsearchProcess(pb.start());
                        esProcess.start();
                    }
                    catch (IOException e)
                    {
                        logger.error("Cannot start Elasticsearch process: {}" + e.getMessage());
                    }
                }
                Thread.sleep(3000);
            }
        }
        catch (InterruptedException e)
        {
            logger.error("Cannot watch Elasticsearch process. {}" + e.getMessage());
        }
    }

    public void stopProcess()
    {
        stopping = true;
        esProcess.stopProcess();
    }
}
