/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.kickstarter.es;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class checks if the process is running. Returns completed = true if it
 * was killed.
 * 
 * @author uwe
 *
 */
public class ElasticsearchProcess extends Thread
{
    private final Logger logger = LoggerFactory.getLogger(ElasticsearchProcess.class.getName());

    public ElasticsearchProcess(Process process)
    {
        super();
        this.process = process;
    }

    private final Process process;
    private volatile boolean complete;

    @Override
    public void run()
    {
        try
        {
            process.waitFor();
            complete = true;
        }
        catch (InterruptedException e)
        {
            logger.error("Cannot monitor Elasticsearch process. {}" + e.getMessage());
        }
    }

    public void stopProcess()
    {
        process.destroy();
    }

    public boolean isComplete()
    {
        return complete;
    }
}
