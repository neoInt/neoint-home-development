/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.parser.file.command;

import org.apache.karaf.shell.api.action.Action;
import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.lifecycle.Reference;
import org.apache.karaf.shell.api.action.lifecycle.Service;

import com.neoint.server.collect.parser.service.FileParserService;

@Command(scope = "neoInt", name = "parse", description = "Parse files in directory.")
@Service
public class ParseDirectoryCommand implements Action
{
    @Argument(index = 0, name = "directory", description = "The path", required = true, multiValued = false)
    String directory = null;

    @Argument(index = 1, name = "recursive", description = "Parse directories recursive", required = true, multiValued = false)
    boolean recursive = false;

    @Reference
    private FileParserService fileParser;

    public Object execute() throws Exception
    {
        if (fileParser != null)
        {
            fileParser.parseFilesFromDirectoryAndIndex(directory, recursive);
        }
        else
        {
            System.out.println("No file parser found");
        }
        return null;
    }

    public FileParserService getFileParser()
    {
        return fileParser;
    }

    public void setFileParser(FileParserService fileParser)
    {
        this.fileParser = fileParser;
    }
}
