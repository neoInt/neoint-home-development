/*******************************************************************************
 * Copyright 2017 neoInt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.neoint.server.collect.email.service.remote.rest;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Loggable;
import com.neoint.server.core.service.NeoIntConfigurationService;

/**
 * This is the RESTful remote interface of the File Parser Service.
 * 
 * @author Uwe Vass
 * 
 */
@Loggable(prepend = true, limit = 5, unit = TimeUnit.SECONDS)
public class EmailCollectorRemoteService
{
    private static final int HTTP_CODE_200 = 200;
    private static final int HTTP_CODE_500 = 500;
    private static final int HTTP_CODE_400 = 400;
    private final Logger logger = LoggerFactory.getLogger(EmailCollectorRemoteService.class.getName());
    private NeoIntConfigurationService emailCollectorConfigService = null;

    /**
     * This operation returns the configuration of the email settings.
     * 
     * @return JSON
     *         {"imapHost":"host","username":"username","password":"password",
     *         "inboxFolder":"INBOX","storagePath":"/tmp","checkInterval":"1",
     *         "onlyUnread":"1"}
     */
    @GET
    @Path("/settings/emailsettings")
    @Produces("application/json; charset=UTF-8")
    public final Response getEmailSettings()
    {
        JSONObject result = new JSONObject();
        String imapHost = "";
        String username = "";
        String password = "";
        String inboxFolder = "";
        String storagePath = "";
        String checkInterval = "5";
        String onlyUnread = "1";
        try
        {
            imapHost = emailCollectorConfigService.getConfiguration("email.imap.host");
            username = emailCollectorConfigService.getConfiguration("email.username");
            password = emailCollectorConfigService.getConfiguration("email.password");
            inboxFolder = emailCollectorConfigService.getConfiguration("email.inbox.folder");
            storagePath = emailCollectorConfigService.getConfiguration("email.storage.path");
            checkInterval = emailCollectorConfigService.getConfiguration("email.check.interval");
            onlyUnread = emailCollectorConfigService.getConfiguration("email.only.unread");

            result.put("imapHost", imapHost);
            result.put("username", username);
            result.put("password", password);
            result.put("inboxFolder", inboxFolder);
            result.put("storagePath", storagePath);
            result.put("checkInterval", checkInterval);
            result.put("onlyUnread", onlyUnread);
        }
        catch (JSONException e)
        {
            logger.error("getEmailSettings: JSON Excpetion {} for settings {},{},{},{},{},{} ", e
                    .getMessage(), imapHost, username, inboxFolder, storagePath, checkInterval, onlyUnread);
            return Response.status(HTTP_CODE_500).entity(result.toString()).build();
        }

        return Response.status(HTTP_CODE_200).entity(result.toString()).build();
    }

    /**
     * This operation sets the configuration of the email settings.
     * 
     * @param parseMessage
     *            - JSON {"imapHost":"host","username":"username","password":
     *            "password",
     *            "inboxFolder":"INBOX","storagePath":"/tmp","checkInterval":
     *            "1", "onlyUnread":"1"}
     * 
     * @return JSON
     *         {"imapHost":"host","username":"username","password":"password",
     *         "inboxFolder":"INBOX","storagePath":"/tmp","checkInterval":"1",
     *         "onlyUnread":"1"}
     */
    @POST
    @Path("/settings/emailsettings")
    @Produces("application/json; charset=UTF-8")
    public final Response setEmailSettings(final String configMessage)
    {
        JSONObject result = new JSONObject();
        final JSONObject configMessageJson;
        String imapHost = "";
        String username = "";
        String password = "";
        String inboxFolder = "";
        String storagePath = "";
        String checkInterval = "5";
        String onlyUnread = "1";
        if (configMessage.length() == 0)
        {
            logger.error("setEmailSettings: Empty message");
            return Response.status(HTTP_CODE_400).entity(result.toString()).build();
        }

        try
        {
            configMessageJson = new JSONObject(configMessage);
            Dictionary<String, Object> properties = new Hashtable<String, Object>();
            properties.put("email.imap.host", configMessageJson.getString("imapHost"));
            properties.put("email.username", configMessageJson.getString("username"));
            properties.put("email.password", configMessageJson.getString("password"));
            properties.put("email.inbox.folder", configMessageJson.getString("inboxFolder"));
            properties.put("email.storage.path", configMessageJson.getString("storagePath"));
            properties.put("email.check.interval", configMessageJson.getString("checkInterval"));
            properties.put("email.only.unread", configMessageJson.getString("onlyUnread"));

            emailCollectorConfigService.setConfigurationProperties(properties);

            imapHost = emailCollectorConfigService.getConfiguration("email.imap.host");
            username = emailCollectorConfigService.getConfiguration("email.username");
            password = emailCollectorConfigService.getConfiguration("email.password");
            inboxFolder = emailCollectorConfigService.getConfiguration("email.inbox.folder");
            storagePath = emailCollectorConfigService.getConfiguration("email.storage.path");
            checkInterval = emailCollectorConfigService.getConfiguration("email.check.interval");
            onlyUnread = emailCollectorConfigService.getConfiguration("email.only.unread");

            result.put("imapHost", imapHost);
            result.put("username", username);
            result.put("password", password);
            result.put("inboxFolder", inboxFolder);
            result.put("storagePath", storagePath);
            result.put("checkInterval", checkInterval);
            result.put("onlyUnread", onlyUnread);
        }
        catch (Exception e)
        {
            logger.error("setEmailSettings: JSON Excpetion {} for settings {},{},{},{},{},{} ", e
                    .getMessage(), imapHost, username, inboxFolder, storagePath, checkInterval, onlyUnread);
            return Response.status(HTTP_CODE_500).entity(result.toString()).build();
        }

        return Response.status(HTTP_CODE_200).entity(result.toString()).build();
    }

    /**
     * The EmailCollectorConfigService
     * 
     * @return EmailCollectorConfigService
     */
    public NeoIntConfigurationService getEmailCollectorConfigService()
    {
        return emailCollectorConfigService;
    }

    /**
     * The EmailCollectorConfigService
     * 
     * @param emailCollectorConfigService
     *            - email collector config service
     */
    public void setEmailCollectorConfigService(NeoIntConfigurationService emailCollectorConfigService)
    {
        this.emailCollectorConfigService = emailCollectorConfigService;
    }
}
