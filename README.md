# README #



## What is neoInt for? ##

### Quick summary
neoInt is a software for data collecting, data discovering and data analytics.
The idea is to have a powerful software for digitizing paper and make it fulltext searchable.

It provides currently the following features:

* Collect data from directories (all kind of files)
* Index Metadata of files and make it searchable
* Monitor directory changes (index, update, delete search content)
* Collect data from a email (IMAP) account including attachments
* OCR software for extracting text of scanned papers (JPG, TIFF) and images
* Web frontend for fulltext search
* Web frontend to display search result on a timeline
* Web frontent to metadata of content
* Web frontent to preview images and download content
* Web frontent to configure neoInt


### Version
Current version is 1.0.0


## Architecture overview ##
![neoInt Architecture](https://bitbucket.org/neoInt/neoint-documentation/downloads/neoInt.png)


## How do I get set up? ##

* Checkout the project
* Run maven on the pom.xml in com.neoint.home.server.cbi
* Result is a ZIP file in the "com.neoint.home.server.cbi/target" folder
* Unzip and start the server by using "neoInt-server/bin/karaf"
* Start the web browser and enter "http://localhost:8080/neoint/index.html"
* Settings: Start the web browser and enter "http://localhost:8080/neoint/settings-file-collector.html"
* Config files are under "neoInt-server/etc"


## We need help!! ##

neoInt is Open Source. And we need help to push it and make it really great.

* You are a frontend developer? HTML5 and Javascript?
* You are a backend developer? Java, Elasticsearch, Neo4J and OSGi?
* You know Maven, CBI Docker quite well?

Great than write an email and send it to uwe@neoint.de. 
Welcome onboard!
